//
//  SideMenuVC.m
//  Dingo
//
//  Created by MAC on 05/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import "SideMenuVC.h"
#import "UserModal.h"
@interface SideMenuVC ()

@end

@implementation SideMenuVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)awakeFromNib
{
    [super awakeFromNib];
    self.navigationController.navigationBar.hidden=YES;
    self.menuPreferredStatusBarStyle = UIStatusBarStyleLightContent;
    self.contentViewShadowColor = [UIColor blackColor];
    self.contentViewShadowOffset = CGSizeMake(0, 0);
    self.contentViewShadowOpacity = 0.6;
    self.contentViewShadowRadius = 12;
    self.contentViewShadowEnabled = YES;
    
    
    if ([[UserModal getUserType] integerValue]==2){

        self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"truckerNav"];
        self.leftMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuVC"];
    }else{
        self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"nav"];
        self.leftMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuVC"];
        
    }
//    self.backgroundImage = [UIImage imageNamed:@"Stars"];
    self.delegate = self;
}
-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden=YES;

}
#pragma mark -
#pragma mark RESideMenu Delegate

- (void)sideMenu:(RESideMenu *)sideMenu willShowMenuViewController:(UIViewController *)menuViewController
{
  //  NSLog(@"willShowMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu didShowMenuViewController:(UIViewController *)menuViewController
{
  //  NSLog(@"didShowMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu willHideMenuViewController:(UIViewController *)menuViewController
{
  //  NSLog(@"willHideMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu didHideMenuViewController:(UIViewController *)menuViewController
{
   // NSLog(@"didHideMenuViewController: %@", NSStringFromClass([menuViewController class]));
}


@end
