//
//  DashboardCell.h
//  Dingo
//
//  Created by MAC on 10/12/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashboardCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iconImgs;

@property (weak, nonatomic) IBOutlet UILabel *menuLabel;

@end
