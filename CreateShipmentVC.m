//
//  CreateShipmentVC.m
//  Dingo
//
//  Created by MAC on 05/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import "CreateShipmentVC.h"
#import "UserModal.h"
#import "ShipmentsVC.h"
#import "SVProgressHUD.h"
#import "WebService.h"
#import "XMLReader.h"
#import "ShipmentPriceVC.h"
@import GooglePlacePicker;
@interface CreateShipmentVC ()<GMSAutocompleteViewControllerDelegate,UIPickerViewDelegate,UIPickerViewDataSource,NSXMLParserDelegate>

{
    BOOL pickup;
    UIDatePicker *datePicker;
    NSDateFormatter *dateFormat;
    UIPickerView *pickerView1;
    NSArray *items;
    NSString *pickUpLat,*pickupLongitute,*dropoffLat,*dropoffLongitute,*addressPickup,*addressDropOff,*sourcePostal,*sourceCountryCode,*destinationPostalCode,*destinationCountryCode;
    NSMutableData *webData;
    BOOL getRate;
    CLGeocoder *geocoder;
    CLLocation  *newLocation;
    NSString* estimatedLinehaulRate,*lowLinehaulRate,*highLinehaulRate,*miles,*averageFuelSurchargeRate,*averageAccessorialCharge,*highLinehaulTotal;
    NSInteger shipperCOst;
    

}
@end

@implementation CreateShipmentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    geocoder=[[CLGeocoder alloc]init];
    // Do any additional setup after loading the view.
    [self setUpDatePicker];
  //  [self getTokenForRateCalculation];
    [UserModal addShadow:_fromBtn];
    [UserModal addShadow:_toBtn];
    [UserModal addShadowToView:_pickUpView];
    [UserModal addShadowToView:_sizeView];
     [UserModal addShadowToView:_weightView];
     [UserModal addShadowToView:_shippmentDetailView];
    
  
    _submitBtn.layer.cornerRadius=_submitBtn.frame.size.height/2;
    _addShipmentBtn.layer.cornerRadius=_addShipmentBtn.frame.size.height/2;
    pickerView1=[[UIPickerView alloc]initWithFrame:CGRectMake(0, 40, self.view.frame.size.width, 200)];
    pickerView1.delegate=self;
    pickerView1.dataSource=self;
    
    [pickerView1 selectRow:0 inComponent:0 animated:YES];
    items=@[@"Load",@"Full"];
}
-(void)viewWillLayoutSubviews{
    [UserModal addGradient:_addShipmentBtn];
    [UserModal addGradient:_submitBtn];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setUpDatePicker{
    dateFormat = [[NSDateFormatter alloc] init];
    
    
    datePicker =[[UIDatePicker alloc] initWithFrame:CGRectMake(0, 40, self.view.frame.size.width, 200)];
    
    datePicker.datePickerMode=UIDatePickerModeDate;
    datePicker.minimumDate=[NSDate date];
    
    [datePicker addTarget:self action:@selector(updateData) forControlEvents:UIControlEventValueChanged];
}
-(void)updateData
{
    
    
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    
    _pickWIndowTF.text= [dateFormat stringFromDate:datePicker.date];
    
    
}
#pragma mark - pickerView DataSouce&Delegates
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return items.count;
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    [self pickerView:pickerView didSelectRow:0 inComponent:0];
    
    return [items objectAtIndex:row];
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    self.loadSizeTF.text=[items objectAtIndex:row];
    
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 40;
}

#pragma mark TextFeild delegates
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField==_pickWIndowTF) {
        _pickWIndowTF.inputView=datePicker;
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        
        _pickWIndowTF.text= [dateFormat stringFromDate:[NSDate date]];

    }
    
    if (textField==_loadSizeTF) {
        _loadSizeTF.inputView=pickerView1;
    }
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    if ([segue.identifier isEqualToString:@"shipments"]) {
//        ShipmentsVC *vc=[segue destinationViewController];
//        vc.fromCreateShipments=YES;
//    }
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//}
#pragma mark- TableView delegates methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)
indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    if (indexPath.row==1) {
        cell.textLabel.text=@"sadfs";

    }
    
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}

- (IBAction)addShipmentAction:(id)sender {
}

- (IBAction)submitAction:(id)sender {
    if ([self validateFeilds]) {
       // [self createShipment];
        [self getTokenForRateCalculation];

    }
}
- (IBAction)dropOffAction:(id)sender{
    pickup=NO;
    [self openGPlaces];
}

- (IBAction)pickUpAction:(id)sender{
    pickup=YES;
    [self openGPlaces];
}
#pragma mark- Google PlaceApi
-(void)openGPlaces{
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    acController.delegate = self;
    [self presentViewController:acController animated:YES completion:nil];
}
- (void)viewController:(GMSAutocompleteViewController *)viewController
didAutocompleteWithPlace:(GMSPlace *)place {
    [self dismissViewControllerAnimated:YES completion:nil];
    // Do something with the selected place.
    [self getZipCodeAndCountryCode:place.coordinate source:pickup];
    if (pickup) {
        addressPickup=place.formattedAddress;
        
        if ([addressPickup isEqualToString:addressDropOff]) {
            addressPickup=nil;
            [_fromBtn setTitle:@"From:" forState:UIControlStateNormal];
            [UserModal showAlert:@"Alert" withMessage:@"Please enter differnt source and destination" withVC:self withPop:NO];
        }else{
            
            [_fromBtn setTitle:[NSString stringWithFormat:@"From: %@",place.formattedAddress] forState:UIControlStateNormal];
            pickUpLat=[NSString stringWithFormat:@"%f",place.coordinate.latitude];
            pickupLongitute=[NSString stringWithFormat:@"%f",place.coordinate.longitude];
        }
        
    }else{
        addressDropOff=place.formattedAddress;
        if ([addressPickup isEqualToString:addressDropOff]) {
            addressDropOff=nil;
            [_toBtn setTitle:@"To:" forState:UIControlStateNormal];
            [UserModal showAlert:@"Alert" withMessage:@"Please enter differnt source and destination" withVC:self withPop:NO];
        }else{
            
            [_toBtn setTitle:[NSString stringWithFormat:@"To: %@",place.formattedAddress] forState:UIControlStateNormal];
            dropoffLat=[NSString stringWithFormat:@"%f",place.coordinate.latitude];
            dropoffLongitute=[NSString stringWithFormat:@"%f",place.coordinate.longitude];
        }
        
        
    }
}

- (void)viewController:(GMSAutocompleteViewController *)viewController
didFailAutocompleteWithError:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:nil];
    // TODO: handle the error.
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)didUpdateAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}
#pragma mark -validation
-(BOOL)validateFeilds{
    NSString *loadSize = [_loadSizeTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
     NSString *loadWeight = [_loadWeightTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString *dateTime = [_pickWIndowTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
     NSString *shpmentDetail = [_shipmntName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (addressPickup.length==0) {
        [UserModal showAlert:@"Alert" withMessage:@"Please Enter Pickup Location" withVC:self withPop:NO];
        return NO;
    }
    if (addressDropOff.length==0) {
        [UserModal showAlert:@"Alert" withMessage:@"Please Enter Dropoff Location" withVC:self withPop:NO];
        return NO;
    }
    if (dateTime.length==0) {
        [UserModal showAlert:@"Alert" withMessage:@"Please Enter Date and time" withVC:self withPop:NO];
        return NO;
    }
    
    if (loadSize.length==0) {
        [UserModal showAlert:@"Alert" withMessage:@"Please Enter Load size" withVC:self withPop:NO];
        return NO;
    }
    if (loadWeight.length==0) {
        [UserModal showAlert:@"Alert" withMessage:@"Please Enter Load weight" withVC:self withPop:NO];
        return NO;
    }
    if (shpmentDetail.length==0) {
        [UserModal showAlert:@"Alert" withMessage:@"Please Enter Name or detail of shipment" withVC:self withPop:NO];
        return NO;
    }
    
    return YES;
}
#pragma mark- web services
-(void)createShipment{
   // [SVProgressHUD show];
    NSDictionary *dic=[[NSDictionary alloc]initWithObjectsAndKeys:[UserModal getUserId],@"user_id",_shipmntName.text,@"shipment_name",addressPickup,@"pickup_address",addressDropOff,@"dropoff_address",pickUpLat,@"pickup_latitude",pickupLongitute,@"pickup_longitude",dropoffLat,@"dropoff_latitude",dropoffLongitute,@"dropoff_longitute",_pickWIndowTF.text,@"pickup_window",_loadSizeTF.text,@"load_size",@"Create Shipment",@"create_shipment",estimatedLinehaulRate,@"estimatedLinehaulRate",lowLinehaulRate,@"lowLinehaulRate",highLinehaulRate,@"highLinehaulRate",miles,@"miles",averageFuelSurchargeRate,@"averageFuelSurchargeRate",@"",@"averageAccessorialCharge",highLinehaulTotal,@"highLinehaulTotal",sourcePostal,@"pickup_zipcode",destinationPostalCode,@"dropoff_zipcode", nil];
    
//    [[WebService sharedInstance]parameters:dic onComplete:^(id responseObject) {
//        [SVProgressHUD dismiss];
//        if ([[responseObject valueForKey:@"status"] integerValue]==1) {
          //  [self performSegueWithIdentifier:@"shipments" sender:nil];
            shipperCOst=highLinehaulTotal.doubleValue-(highLinehaulTotal.doubleValue*0.05);
            ShipmentPriceVC *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"ShipmentPriceVC"];
            vc.estimatedCost=[NSString stringWithFormat:@"%ld",(long)shipperCOst];
    vc.postDic=dic;
            [self.navigationController pushViewController:vc animated:YES];
//        }else{
//            
//        }
//    } onError:^(NSError *error) {
//        [SVProgressHUD dismiss];
//        [UserModal showAlert:@"Error!!" withMessage:error.localizedDescription withVC:self withPop:NO];
//        
//    }];
}
-(void)getTokenForRateCalculation{
    NSString *soapMessage=[NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                           "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tcor=\"http://www.tcore.com/TcoreHeaders.xsd\" xmlns:tcor1=\"http://www.tcore.com/TcoreTypes.xsd\" xmlns:tfm=\"http://www.tcore.com/TfmiFreightMatching.xsd\">\n"
                           "<soapenv:Header>\n"
                           "<tcor:sessionHeader>\n"
                           "<tcor:sessionToken>\n"
                           "<tcor1:primary></tcor1:primary>\n"
                           "<tcor1:secondary></tcor1:secondary>\n"
                         // " <!--Optional:-->"
                           "</tcor:sessionToken>\n"
                           "</tcor:sessionHeader>\n"
                           "<tcor:correlationHeader>\n"
                           //"<!--Optional:-->\n"
                           "<tcor:Id></tcor:Id>\n"
                           "</tcor:correlationHeader>\n"
                           "<tcor:applicationHeader>\n"
                           "<tcor:application></tcor:application>\n"
                           "<tcor:applicationVersion></tcor:applicationVersion>\n"
                           "</tcor:applicationHeader>\n"
                           "</soapenv:Header>\n"
                           "<soapenv:Body>\n"
                           "<tfm:loginRequest>\n"
                           "<tfm:loginOperation>\n"
                           "<tfm:loginId>ryder_cnx1</tfm:loginId>\n"
                           "<tfm:password>ryder1</tfm:password>\n"
                           "<tfm:thirdPartyId>dl</tfm:thirdPartyId>\n"
                           //"<!--Optional:-->"
                           "<tfm:apiVersion>1</tfm:apiVersion>\n"
                           "</tfm:loginOperation>\n"
                           "</tfm:loginRequest>\n"
                           "</soapenv:Body>\n"
                           "</soapenv:Envelope>"];
    
    
    
    NSURL *url = [NSURL URLWithString:@"http://cnx.test.dat.com:9280/TfmiRequest"];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
  //  [theRequest addValue: @"http://cnx.test.dat.com:9280/TfmiRequest/tfm:loginRequest" forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    [SVProgressHUD show];
    
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    if( theConnection )
    {
        webData = [NSMutableData data] ;
    }
    else
    {
        NSLog(@"theConnection is NULL");
    }
    getRate=NO;
}
-(void)getRate:(NSString*)primaryToken token:(NSString*)secondaryToken{
    NSString *soapMessage=[NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                           "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tcor=\"http://www.tcore.com/TcoreHeaders.xsd\" xmlns:tcor1=\"http://www.tcore.com/TcoreTypes.xsd\" xmlns:tfm=\"http://www.tcore.com/TfmiRates.xsd\" xmlns:tfm1=\"http://www.tcore.com/TfmiFreightMatching.xsd\">\n"
                           "<soapenv:Header>\n"
                           "<tcor:sessionHeader>\n"
                           "<tcor:sessionToken>\n"
                           "<tcor1:primary>%@</tcor1:primary>\n"
                           "<tcor1:secondary>%@</tcor1:secondary>\n"
                           "</tcor:sessionToken>\n"
                           "</tcor:sessionHeader>\n"
                           "<tcor:correlationHeader>\n"
                           "</tcor:correlationHeader>\n"
                           "<tcor:applicationHeader>\n"
                           "<tcor:application>DL</tcor:application>\n"
                           "<tcor:applicationVersion>1</tcor:applicationVersion>\n"
                           "</tcor:applicationHeader>\n"
                           "</soapenv:Header>\n"
                           "<soapenv:Body>\n"
                           "<tfm:lookupRateRequest>"
                           //!--1 to 50 repetitions:-->
                           "<tfm:lookupRateOperations>\n"
                           "<tfm:equipment>Vans</tfm:equipment>\n"
                           "<tfm:origin>\n"
                           // !--You have a CHOICE of the next 5 items at this level-->
                           "<tfm1:postalCode>\n"
                           "<tfm1:country>%@</tfm1:country>\n"
                           "<tfm1:code>%@</tfm1:code>\n"
                           "</tfm1:postalCode>\n"
                           "</tfm:origin>\n"
                           "<tfm:destination>\n"
                           //  "<!--You have a CHOICE of the next 5 items at this level-->"
                           "<tfm1:postalCode>\n"
                           "<tfm1:country>%@</tfm1:country>\n"
                           "<tfm1:code>%@</tfm1:code>\n"
                           "</tfm1:postalCode>\n"
                           "</tfm:destination>"
                           //   "<!--Optional:-->\n"
                           "<tfm:includeSpotRate>1</tfm:includeSpotRate>"
                           //  "<!--Optional:-->"
                         //  "<tfm:includeContrhttps://rateview.dat.com/Accounts/API/Session/Rate>1</tfm:includeContractRate>\n"
                           //  "<!--Optional:-->"
                           " <tfm:includeMyRate>1</tfm:includeMyRate>"
                           "</tfm:lookupRateOperations>"
                           "</tfm:lookupRateRequest>"
                           "</soapenv:Body>"
                           "</soapenv:Envelope>",primaryToken,secondaryToken,sourceCountryCode,sourcePostal,destinationCountryCode,destinationPostalCode];
    
    
    
    NSURL *url = [NSURL URLWithString:@"http://cnx.test.dat.com:9280/TfmiRequest"];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
   /// [theRequest addValue: @"Accounts/API/Session/Rate/tfm:lookupRateRequest" forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    if( theConnection )
    {
        webData = [NSMutableData data] ;
    }
    else
    {
        NSLog(@"theConnection is NULL");
    }
    getRate=YES;
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [webData setLength: 0];
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [webData appendData:data];
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"ERROR with theConenction");
    [SVProgressHUD showErrorWithStatus:error.localizedDescription];
    
    
    
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    NSString *theXML = [[NSString alloc] initWithBytes: [webData mutableBytes] length:[webData length] encoding:NSUTF8StringEncoding];
    NSLog(@"%@",theXML);
    
    NSData *osman = [theXML dataUsingEncoding:NSUTF8StringEncoding];
    
    NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:osman];
    
    // Don't forget to set the delegate!
    xmlParser.delegate = self;
    
    
    NSError *parseError = nil;
    NSDictionary *xmlDictionary = [XMLReader dictionaryForXMLString:theXML error:&parseError];
    NSLog(@" %@", xmlDictionary);
    if (!getRate) {
        NSString *primaryToken=[xmlDictionary valueForKeyPath:@"soapenv:Envelope.soapenv:Body.tfm:loginResponse.tfm:loginResult.tfm:loginSuccessData.tfm:token.tcor:primary.text"];
        NSString *secondryToken=[xmlDictionary valueForKeyPath:@"soapenv:Envelope.soapenv:Body.tfm:loginResponse.tfm:loginResult.tfm:loginSuccessData.tfm:token.tcor:secondary.text"];
        
        if (primaryToken) {
            [self getRate:primaryToken token:secondryToken];
        }
    }else{
        estimatedLinehaulRate=[xmlDictionary valueForKeyPath:@"soapenv:Envelope.soapenv:Body.tfm:lookupRateResponse.tfm:lookupRateResults.tfm:lookupRateSuccessData.tfm:spotRate.tfm:estimatedLinehaulRate.text"];
        lowLinehaulRate=[xmlDictionary valueForKeyPath:@"soapenv:Envelope.soapenv:Body.tfm:lookupRateResponse.tfm:lookupRateResults.tfm:lookupRateSuccessData.tfm:spotRate.tfm:lowLinehaulRate.text"];
        highLinehaulRate=[xmlDictionary valueForKeyPath:@"soapenv:Envelope.soapenv:Body.tfm:lookupRateResponse.tfm:lookupRateResults.tfm:lookupRateSuccessData.tfm:spotRate.tfm:highLinehaulRate.text"];
        miles=[xmlDictionary valueForKeyPath:@"soapenv:Envelope.soapenv:Body.tfm:lookupRateResponse.tfm:lookupRateResults.tfm:lookupRateSuccessData.tfm:miles.text"];
        averageFuelSurchargeRate=[xmlDictionary valueForKeyPath:@"soapenv:Envelope.soapenv:Body.tfm:lookupRateResponse.tfm:lookupRateResults.tfm:lookupRateSuccessData.tfm:spotRate.tfm:averageFuelSurchargeRate.text"];
//        averageAccessorialCharge=[xmlDictionary valueForKeyPath:@"soapenv:Envelope.soapenv:Body.tfm:lookupRateResponse.tfm:lookupRateResults.tfm:lookupRateSuccessData.tfm:miles.text"];
        [SVProgressHUD dismiss];
        highLinehaulTotal=[xmlDictionary valueForKeyPath:@"soapenv:Envelope.soapenv:Body.tfm:lookupRateResponse.tfm:lookupRateResults.tfm:lookupRateSuccessData.tfm:spotRate.tfm:highLinehaulTotal.text"];
        if (highLinehaulTotal==nil) {
            [UserModal showAlert:@"Alert" withMessage:@"Please enter source and destination with in United state" withVC:self withPop:NO];
        }else{
            [self createShipment];

        }
    }
   
}

-(void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    
    
    
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    //       currentElement=elementName;
}
-(void)getZipCodeAndCountryCode:(CLLocationCoordinate2D)cordinates source:(BOOL)source{
    newLocation= [[CLLocation alloc] initWithLatitude:cordinates.latitude longitude:cordinates.longitude];
    [geocoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray *placemarks, NSError *error)
     {
         
         if (placemarks && placemarks.count > 0)
         {
             CLPlacemark *placemark = placemarks[0];
             
             NSDictionary *addressDictionary =placemark.addressDictionary;
             if (source) {
                 sourcePostal=[addressDictionary valueForKey:@"ZIP"];
                 sourceCountryCode=[addressDictionary valueForKey:@"CountryCode"];
             }else{
                 destinationPostalCode=[addressDictionary valueForKey:@"ZIP"];
                 destinationCountryCode=[addressDictionary valueForKey:@"CountryCode"];
             }
            // NSArray*formattedAddressLines = [addressDictionary objectForKey:@"FormattedAddressLines"];
      //   NSString*    address = [formattedAddressLines objectAtIndex:0];
}
     }];

}

@end
