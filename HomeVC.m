//
//  HomeVC.m
//  Dingo
//
//  Created by MAC on 05/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import "HomeVC.h"
#import "UserModal.h"
#import "Annotation.h"
#import "TrackShipmentVC.h"
#import "SVProgressHUD.h"
#import "WebService.h"
#import "XMLReader.h"
#import "PointAnnotation.h"
@interface HomeVC ()<MKMapViewDelegate,NSXMLParserDelegate,CLLocationManagerDelegate>
{
    NSMutableData *webData;
}
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLLocation* currentLocation;
@end

@implementation HomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _createShipmentBtn.layer.cornerRadius=self.view.frame.size.height*0.036;
    [UserModal addShadow:_createShipmentBtn];
  //  [self addAllPins];
    
    self.mapView.delegate =  self;
    self.mapView.showsUserLocation = YES;
    _mapView.userTrackingMode=YES;
    if ([CLLocationManager locationServicesEnabled] )
    {
        if (self.locationManager == nil )
        {
            self.locationManager = [[CLLocationManager alloc] init];
            self.locationManager.delegate = self;
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            self.locationManager.distanceFilter = kCLDistanceFilterNone;
        }
        
        [self.locationManager startUpdatingLocation];
        [self mapLocationSet:self.locationManager.location];
    }
    [self addAllPins];


}
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    //CLLocationCoordinate2D loc = [userLocation coordinate];
//    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(loc, 250, 250);
//    [_mapView setRegion:region animated:YES];
   // [self mapLocationSet:userLocation ];

   
}
-(void)mapLocationSet:(CLLocation*)locationn
{
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.05;
    span.longitudeDelta = 0.05;
    region.span = span;
    region.center =locationn.coordinate;
    [self.mapView setRegion:region animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addAllPins
{
    self.mapView.delegate=self;
    
    NSArray *name=[[NSArray alloc]initWithObjects:
                   @"VelaCherry",
                   @"Perungudi",
                   @"Tharamani", nil];
    
    NSMutableArray *arrCoordinateStr = [[NSMutableArray alloc] initWithCapacity:name.count];
    
    [arrCoordinateStr addObject:@"12.970760345459, 80.2190093994141"];
    [arrCoordinateStr addObject:@"12.9752297537231, 80.2313079833984"];
    [arrCoordinateStr addObject:@"12.9788103103638, 80.2412414550781"];
    
    for(int i = 0; i < name.count; i++)
    {
        [self addPinWithTitle:name[i] AndCoordinate:arrCoordinateStr[i] tag:i];
    }
}


-(void)addPinWithTitle:(NSString *)title AndCoordinate:(NSString *)strCoordinate tag:(NSInteger)tag;
{
    PointAnnotation *mapPin = [[PointAnnotation alloc] init];
    mapPin.tags=tag;
    
    // clear out any white space
    strCoordinate = [strCoordinate stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    // convert string into actual latitude and longitude values
    NSArray *components = [strCoordinate componentsSeparatedByString:@","];
    
    double latitude = [components[0] doubleValue];
    double longitude = [components[1] doubleValue];
    
    // setup the map pin with all data and add to map view
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longitude);
    
    mapPin.title = title;
    mapPin.coordinate = coordinate;
    
    [self.mapView addAnnotation:mapPin];
}
- (MKAnnotationView *)mapView:(MKMapView *)map viewForAnnotation:(id <MKAnnotation>)annotation
{
    if (annotation == _mapView.userLocation) return nil;
    
    Annotation *pin = (Annotation *) [_mapView dequeueReusableAnnotationViewWithIdentifier: @"CustomAnnotation"];
    PointAnnotation *an=(PointAnnotation*)annotation;
    if (pin == nil)
    {
        pin = [[Annotation alloc] initWithAnnotation: annotation reuseIdentifier: @"CustomAnnotation"];
    }
    else
    {
        pin.annotation = annotation;
    }
    pin.tag=an.tags;
    pin.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
  //  pin.tintColor = MKPinAnnotationColorRed;
   // pin.animatesDrop = YES;
    [pin setEnabled:YES];
  //  [pin setCanShowCallout:YES];
     pin.image = [UIImage imageNamed:@"address"];
    return pin;
    
}
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    NSLog(@"%f",[[view annotation] coordinate].latitude);
     NSLog(@"%f",[[view annotation] coordinate].longitude);
    
    UIAlertController *alertC = [UIAlertController alertControllerWithTitle:@"Track Shipment" message:@"Do you want to Track this shipment" preferredStyle:UIAlertControllerStyleAlert];
    
    
        [alertC addAction:[UIAlertAction actionWithTitle:@"Track"  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            TrackShipmentVC *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"TrackShipmentVC"];
            vc.fromHome=YES;
            [self.navigationController pushViewController:vc animated:YES];
        }]];
    
        [alertC addAction:[UIAlertAction actionWithTitle:@"Cancel"  style:UIAlertActionStyleCancel handler:^(UIAlertAction * action){
            
        }]];
        
        [self presentViewController: alertC animated:YES completion:nil];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)createShipmentAction:(id)sender {
    [self performSegueWithIdentifier:@"addShipment" sender:nil];
}


@end
