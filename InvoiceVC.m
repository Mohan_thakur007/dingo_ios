//
//  InvoiceVC.m
//  Dingo
//
//  Created by MAC on 07/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import "InvoiceVC.h"
#import "UserModal.h"
@interface InvoiceVC ()

@end

@implementation InvoiceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    for (UIButton *btn in [self.view subviews]) {
        if ([btn isKindOfClass:[UIButton class]]) {
            [UserModal addBorder:btn color:[UserModal staticAppColor]];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
