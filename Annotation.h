//
//  Annotation.h
//  Dingo
//
//  Created by MAC on 08/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface Annotation : MKAnnotationView<MKAnnotation>{
    CLLocationCoordinate2D coordinate;
}
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

// add an init method so you can set the coordinate property on startup
- (id) initWithCoordinate:(CLLocationCoordinate2D)coord;


@end
