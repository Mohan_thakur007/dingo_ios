//
//  ForgotPasswordVC.m
//  Dingo
//
//  Created by MAC on 06/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//0

#import "ForgotPasswordVC.h"
#import "UserModal.h"
#import "SVProgressHUD.h"
#import "WebService.h"
@interface ForgotPasswordVC ()
{
    UIAlertAction *actionOk;
    
}
@end

@implementation ForgotPasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _submitBtn.layer.cornerRadius=_submitBtn.frame.size.height/2;
}
-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden=NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillLayoutSubviews{
    [UserModal addGradient:_submitBtn];
    [UserModal addLayer:_emailTF];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)submitAction:(id)sender {
    if ([self validateFields]) {
        [self forgotPassword];
    }
}
-(void)showAlert{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Change Password"
                                                                             message:@"Please check your email for security code"
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Enter Email";
        textField.text=_emailTF.text;
        textField.keyboardType = UIKeyboardTypeEmailAddress;
        //[textField addTarget:self action:@selector(textDidChange:) forControlEvents:UIControlEventEditingChanged];
        
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Enter Securiy Code";
        textField.keyboardType = UIKeyboardTypeEmailAddress;
        // [textField addTarget:self action:@selector(textDidChange:) forControlEvents:UIControlEventEditingChanged];
        
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Enter New Password";
        textField.keyboardType = UIKeyboardTypeEmailAddress;
        //  [textField addTarget:self action:@selector(textDidChange:) forControlEvents:UIControlEventEditingChanged];
        
    }];
    
    actionOk = [UIAlertAction actionWithTitle:@"Reset"
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * _Nonnull action)
                                       {
                                          [self setNewPass:[[alertController textFields][0] text] securityCOde:[[alertController textFields][1] text] newPass:[[alertController textFields][2] text]];
                                      }];
    UIAlertAction*  cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                      style:UIAlertActionStyleDestructive
                                                    handler:^(UIAlertAction * _Nonnull action){
                                                       
                                                    }];
    
    
    //  actionOk.enabled=NO;
    [alertController addAction:cancel];
    
    [alertController addAction:actionOk];
    
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}
#pragma mark- validations
-(BOOL)validateFields{
    NSString *email= [_emailTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    
    NSString *emailRegEx = @"[0-9a-z._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    
    
    if([emailTest evaluateWithObject:email] != YES && [email length]==0){
        
        [UserModal showAlert:@"Alert" withMessage:@"Please enter valid email address" withVC:self withPop:NO];
        return NO;
    }
    
    return YES;
}
#pragma mark WebServices
-(void)setNewPass:(NSString  *)email securityCOde:(NSString *)code newPass:(NSString *)newpass{
    
    [SVProgressHUD show];
    
    NSDictionary *dic=[[NSDictionary alloc]initWithObjectsAndKeys:_emailTF.text,@"email",code,@"check_code",newpass,@"new_password",@"Reset Password",@"reset_password", nil];
    
    [[WebService sharedInstance]parameters:dic onComplete:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([[responseObject valueForKey:@"status"]integerValue]==1) {
            
            [UserModal showAlert:@"Message" withMessage:@"Your password successfully changed" withVC:self withPop:YES];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [UserModal showAlert:@"Alert" withMessage:@"Security code deosn't match" withVC:self withPop:YES];

        }
    } onError:^(NSError *error) {
        [SVProgressHUD dismiss];
        [UserModal showAlert:@"Error!!" withMessage:error.localizedDescription withVC:self withPop:YES];
    }];
    
}

-(void)forgotPassword{
    [SVProgressHUD show];
    NSDictionary *dic=[[NSDictionary alloc]initWithObjectsAndKeys:_emailTF.text,@"email",@"Forgot Password",@"forgot_password", nil];
    
    [[WebService sharedInstance]parameters:dic onComplete:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([[responseObject valueForKey:@"status"]integerValue]==1) {
            [self showAlert];
            
        }else{
            [UserModal showAlert:@"Alert" withMessage:[responseObject valueForKey:@"message"] withVC:self withPop:NO];
            
        }
    } onError:^(NSError *error) {
        [SVProgressHUD dismiss];
        [UserModal showAlert:@"Error!!" withMessage:error.localizedDescription withVC:self withPop:NO];
        
    }];
    
}

@end
