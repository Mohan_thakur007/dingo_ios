//
//  PointAnnotation.h
//  Dingo
//
//  Created by mac on 7/7/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface PointAnnotation : MKPointAnnotation
@property NSInteger tags;
@end
