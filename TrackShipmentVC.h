//
//  TrackShipmentVC.h
//  Dingo
//
//  Created by MAC on 06/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@interface TrackShipmentVC : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *trackBtn;
@property (weak, nonatomic) IBOutlet UITextField *shipmentNumberTF;
- (IBAction)trackAction:(id)sender;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@property (strong, nonatomic) IBOutlet UIView *shadowView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *leftBatBtn;
@property BOOL fromHome;
@property NSString* shipmentNumber;
@property (weak, nonatomic) IBOutlet UILabel *locLbl;
@property (weak, nonatomic) IBOutlet UIView *locView;
@end
