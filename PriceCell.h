//
//  PriceCell.h
//  Dingo
//
//  Created by MAC on 09/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PriceCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *selectionBtn;
@property (strong, nonatomic) IBOutlet UIImageView *selectionImg;

@end
