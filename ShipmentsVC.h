//
//  ShipmentsVC.h
//  Dingo
//
//  Created by MAC on 05/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShipmentsVC : UIViewController

- (IBAction)statusAction:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *okBtn;
- (IBAction)okAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *menuBtn;
@property BOOL fromCreateShipments;
- (IBAction)trackAction:(id)sender;
- (IBAction)detailAction:(id)sender;
@property UIRefreshControl *refreshControl;

@end
