//
//  SignUpVC.m
//  Dingo
//
//  Created by MAC on 03/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import "SignUpVC.h"
#import "UserModal.h"
#import "SideMenuVC.h"
#import "WebService.h"
#import "SVProgressHUD.h"
@interface SignUpVC ()<UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    UIPickerView *pickerView1;
    NSArray *items;
    BOOL pickImage;
    NSString *callingCode;
}
@end

@implementation SignUpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setDefaultCountryCode];
    // Do any additional setup after loading the view.
    _profilePic.layer.cornerRadius=_profilePic.frame.size.height/2;
    _profilePic.clipsToBounds=YES;
    pickerView1=[[UIPickerView alloc]initWithFrame:CGRectMake(0, 40, self.view.frame.size.width, 200)];
    pickerView1.delegate=self;
    pickerView1.dataSource=self;
    [pickerView1 selectRow:0 inComponent:0 animated:YES];
    items=@[@"Shipper",@"Trucker"];
    _profilePic.layer.cornerRadius=self.view.frame.size.height*0.07;
    _profilePic.clipsToBounds=YES;
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandlerMethod:)];
    tapRecognizer.numberOfTapsRequired=1;
    _profilePic.userInteractionEnabled=YES;
    [_profilePic addGestureRecognizer:tapRecognizer];
}
- (NSDictionary *)getCountryCodeDictionary {
    return [NSDictionary dictionaryWithObjectsAndKeys:@"972", @"IL",
            @"93", @"AF", @"355", @"AL", @"213", @"DZ", @"1", @"AS",
            @"376", @"AD", @"244", @"AO", @"1", @"AI", @"1", @"AG",
            @"54", @"AR", @"374", @"AM", @"297", @"AW", @"61", @"AU",
            @"43", @"AT", @"994", @"AZ", @"1", @"BS", @"973", @"BH",
            @"880", @"BD", @"1", @"BB", @"375", @"BY", @"32", @"BE",
            @"501", @"BZ", @"229", @"BJ", @"1", @"BM", @"975", @"BT",
            @"387", @"BA", @"267", @"BW", @"55", @"BR", @"246", @"IO",
            @"359", @"BG", @"226", @"BF", @"257", @"BI", @"855", @"KH",
            @"237", @"CM", @"1", @"CA", @"238", @"CV", @"345", @"KY",
            @"236", @"CF", @"235", @"TD", @"56", @"CL", @"86", @"CN",
            @"61", @"CX", @"57", @"CO", @"269", @"KM", @"242", @"CG",
            @"682", @"CK", @"506", @"CR", @"385", @"HR", @"53", @"CU",
            @"537", @"CY", @"420", @"CZ", @"45", @"DK", @"253", @"DJ",
            @"1", @"DM", @"1", @"DO", @"593", @"EC", @"20", @"EG",
            @"503", @"SV", @"240", @"GQ", @"291", @"ER", @"372", @"EE",
            @"251", @"ET", @"298", @"FO", @"679", @"FJ", @"358", @"FI",
            @"33", @"FR", @"594", @"GF", @"689", @"PF", @"241", @"GA",
            @"220", @"GM", @"995", @"GE", @"49", @"DE", @"233", @"GH",
            @"350", @"GI", @"30", @"GR", @"299", @"GL", @"1", @"GD",
            @"590", @"GP", @"1", @"GU", @"502", @"GT", @"224", @"GN",
            @"245", @"GW", @"595", @"GY", @"509", @"HT", @"504", @"HN",
            @"36", @"HU", @"354", @"IS", @"91", @"IN", @"62", @"ID",
            @"964", @"IQ", @"353", @"IE", @"972", @"IL", @"39", @"IT",
            @"1", @"JM", @"81", @"JP", @"962", @"JO", @"77", @"KZ",
            @"254", @"KE", @"686", @"KI", @"965", @"KW", @"996", @"KG",
            @"371", @"LV", @"961", @"LB", @"266", @"LS", @"231", @"LR",
            @"423", @"LI", @"370", @"LT", @"352", @"LU", @"261", @"MG",
            @"265", @"MW", @"60", @"MY", @"960", @"MV", @"223", @"ML",
            @"356", @"MT", @"692", @"MH", @"596", @"MQ", @"222", @"MR",
            @"230", @"MU", @"262", @"YT", @"52", @"MX", @"377", @"MC",
            @"976", @"MN", @"382", @"ME", @"1", @"MS", @"212", @"MA",
            @"95", @"MM", @"264", @"NA", @"674", @"NR", @"977", @"NP",
            @"31", @"NL", @"599", @"AN", @"687", @"NC", @"64", @"NZ",
            @"505", @"NI", @"227", @"NE", @"234", @"NG", @"683", @"NU",
            @"672", @"NF", @"1", @"MP", @"47", @"NO", @"968", @"OM",
            @"92", @"PK", @"680", @"PW", @"507", @"PA", @"675", @"PG",
            @"595", @"PY", @"51", @"PE", @"63", @"PH", @"48", @"PL",
            @"351", @"PT", @"1", @"PR", @"974", @"QA", @"40", @"RO",
            @"250", @"RW", @"685", @"WS", @"378", @"SM", @"966", @"SA",
            @"221", @"SN", @"381", @"RS", @"248", @"SC", @"232", @"SL",
            @"65", @"SG", @"421", @"SK", @"386", @"SI", @"677", @"SB",
            @"27", @"ZA", @"500", @"GS", @"34", @"ES", @"94", @"LK",
            @"249", @"SD", @"597", @"SR", @"268", @"SZ", @"46", @"SE",
            @"41", @"CH", @"992", @"TJ", @"66", @"TH", @"228", @"TG",
            @"690", @"TK", @"676", @"TO", @"1", @"TT", @"216", @"TN",
            @"90", @"TR", @"993", @"TM", @"1", @"TC", @"688", @"TV",
            @"256", @"UG", @"380", @"UA", @"971", @"AE", @"44", @"GB",
            @"1", @"US", @"598", @"UY", @"998", @"UZ", @"678", @"VU",
            @"681", @"WF", @"967", @"YE", @"260", @"ZM", @"263", @"ZW",
            @"591", @"BO", @"673", @"BN", @"61", @"CC", @"243", @"CD",
            @"225", @"CI", @"500", @"FK", @"44", @"GG", @"379", @"VA",
            @"852", @"HK", @"98", @"IR", @"44", @"IM", @"44", @"JE",
            @"850", @"KP", @"82", @"KR", @"856", @"LA", @"218", @"LY",
            @"853", @"MO", @"389", @"MK", @"691", @"FM", @"373", @"MD",
            @"258", @"MZ", @"970", @"PS", @"872", @"PN", @"262", @"RE",
            @"7", @"RU", @"590", @"BL", @"290", @"SH", @"1", @"KN",
            @"1", @"LC", @"590", @"MF", @"508", @"PM", @"1", @"VC",
            @"239", @"ST", @"252", @"SO", @"47", @"SJ", @"963", @"SY",
            @"886", @"TW", @"255", @"TZ", @"670", @"TL", @"58", @"VE",
            @"84", @"VN", @"1", @"VG", @"1", @"VI", nil];
}
-(void)setDefaultCountryCode{
    NSString *countryIdentifier = [[NSLocale currentLocale] objectForKey: NSLocaleCountryCode];
   _phNumberTF.text= callingCode=[NSString stringWithFormat:@"+%@",[[self getCountryCodeDictionary] objectForKey:countryIdentifier]];
}
-(void)viewWillLayoutSubviews{
    for (UITextField *TF in [self.innerView subviews]) {
        if ([TF isKindOfClass:[UITextField class]]) {
            if (TF!=_typeOfUser) {
                [UserModal addLayer:TF];
            }
         
        }
    }
    _submitBtn.layer.cornerRadius=_submitBtn.frame.size.height/2;
    [UserModal addGradient:_submitBtn];
}
-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden=NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField==_typeOfUser) {
        _typeOfUser.inputView=pickerView1;
    }
}
-(BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (textField==_phNumberTF) {
        if(text.length > 10+callingCode.length) {
            return NO;
        }
        if (text.length<callingCode.length) {
            return NO;
        }
    }
    return YES;
}
-(void)gestureHandlerMethod:(UITapGestureRecognizer*)sender {
    [self imagePicker];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info;
{
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    _profilePic.image=image;
    pickImage=YES;
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}
- (IBAction)submitAction:(id)sender {
    
    if ([self validateFields]) {
        [self signUp];
    }
  
}

#pragma mark - pickerView DataSouce&Delegates
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return items.count;
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
     [self pickerView:pickerView didSelectRow:0 inComponent:0];

    return [items objectAtIndex:row];
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    self.typeOfUser.text=[items objectAtIndex:row];
    
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 40;
}

- (IBAction)cameraAction:(id)sender {
    [self imagePicker];
}
-(void)imagePicker{
    UIAlertController *alertC = [UIAlertController alertControllerWithTitle:@"Select Method" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    [alertC addAction:[UIAlertAction actionWithTitle:@"Camera"  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePickerController.delegate = self;
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }]];
    
    [alertC addAction:[UIAlertAction actionWithTitle:@"Gallery"  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePickerController.delegate = self;
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }]];
    [alertC addAction:[UIAlertAction actionWithTitle:@"Cancel"  style:UIAlertActionStyleCancel handler:^(UIAlertAction * action){
        
    }]];
    
    [self presentViewController: alertC animated:YES completion:nil];
}
#pragma mark- validate fields
-(BOOL)validateFields{
    NSString *emailId = [_emailTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *password = [_passwordTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *firstName = [_firstName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *lastName = [_lastNameTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *buisnessName = [_buisnessNameTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *mcNumber = [_mcNumberTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *dotNumber = [_dotNumberTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *poc = [_pocTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *phoneNumber = [_phNumberTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *userType = [_typeOfUser.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString *emailRegEx = @"[0-9a-zA-Z._%+-]+@[a-z0-9A_Z.-]+\\.[A_Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    if ([_typeOfUser.text isEqualToString:@"Trucker"]) {
        if (emailId.length==0||password.length==0||firstName.length==0||lastName.length==0||buisnessName.length==0||mcNumber.length==0||dotNumber.length==0||poc.length==0||phoneNumber.length==0||userType.length==0) {
            [UserModal showAlert:@"Alert" withMessage:@"Please fill empty feilds" withVC:self withPop:NO];
            return NO;
        }
    }else{
        if (emailId.length==0||password.length==0||firstName.length==0||lastName.length==0||buisnessName.length==0||poc.length==0||phoneNumber.length==0||userType.length==0) {
            [UserModal showAlert:@"Alert" withMessage:@"Please fill empty feilds" withVC:self withPop:NO];
            return NO;
        }
    }
    if([emailTest evaluateWithObject:emailId] != YES && [emailId length]!=0){
        [UserModal showAlert:@"Alert" withMessage:@"Please Enter valid email" withVC:self withPop:NO];
        return NO;
        
    }
    if (phoneNumber.length<10) {
        [UserModal showAlert:@"Alert" withMessage:@"Please Enter valid contact number" withVC:self withPop:NO];

        return NO;
    }
    if ([_typeOfUser.text isEqualToString:@"Trucker"]) {
        if (!pickImage) {
            [UserModal showAlert:@"Alert" withMessage:@"Please Select Profile Picture" withVC:self withPop:NO];
            return NO;
        }
    }
    return YES;
}
#pragma mark- webServices
-(void)signUp{
    NSString *type;
    if ([_typeOfUser.text isEqualToString:@"Trucker"]) {
        type=@"2";
    }else{
        type=@"1";
    }
    [SVProgressHUD show];
    NSDictionary *dic=[[NSDictionary alloc]initWithObjectsAndKeys:_firstName.text,@"first_name",_lastNameTF.text,@"last_name",[NSString stringWithFormat:@"%@ %@",_firstName.text,_lastNameTF.text],@"user_name",_emailTF.text,@"email",_passwordTF.text,@"password",_buisnessNameTF.text,@"business_name",_mcNumberTF.text,@"mc_number",_dotNumberTF.text,@"dot_number",_pocTF.text,@"poc",_phNumberTF.text,@"phone_number",@"",@"device_id",@"",@"device_type",type,@"user_type",@"Sign Up",@"sign_up" ,nil];
    
    [[WebService sharedInstance]parameters:dic image:UIImageJPEGRepresentation(_profilePic.image, 0.5) imageparameter:@"user_image" onComplete:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([[responseObject valueForKey:@"status"]integerValue]==1) {
            if ([_typeOfUser.text isEqualToString:@"Trucker"]) {
                [[UserModal sharedInstance]setUserType:@"Trucker"];
            }
            [UserModal saveUserName:[responseObject valueForKeyPath:@"result.first_name"] lastName:[responseObject valueForKeyPath:@"result.last_name"] profilePic:[NSString stringWithFormat:@"%@%@",[responseObject valueForKey:@"url"],[responseObject valueForKeyPath:@"result.user_image"]] email:[responseObject valueForKeyPath:@"result.email"] phNumber:[responseObject valueForKeyPath:@"result.phone_number"] userId:[responseObject valueForKeyPath:@"result.id"]userType:[responseObject valueForKeyPath:@"result.user_type"]];
            SideMenuVC *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"SideMenuVC"];
            [self.navigationController pushViewController:vc animated:YES];
        }else{
            [UserModal showAlert:@"Alert" withMessage:[responseObject valueForKey:@"message"] withVC:self withPop:NO];
        }
       
        
    } onError:^(NSError *error) {
        [SVProgressHUD dismiss];
        [UserModal showAlert:@"Error!!" withMessage:error.localizedDescription withVC:self withPop:NO];
        
    }];
}
@end
