//
//  SignUpVC.h
//  Dingo
//
//  Created by MAC on 03/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignUpVC : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *submitBtn;
- (IBAction)submitAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *innerView;
@property (strong, nonatomic) IBOutlet UITextField *typeOfUser;
@property (strong, nonatomic) IBOutlet UIImageView *profilePic;
- (IBAction)cameraAction:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *lastNameTF;
@property (strong, nonatomic) IBOutlet UITextField *buisnessNameTF;
@property (strong, nonatomic) IBOutlet UITextField *mcNumberTF;
@property (strong, nonatomic) IBOutlet UITextField *dotNumberTF;
@property (strong, nonatomic) IBOutlet UITextField *pocTF;
@property (strong, nonatomic) IBOutlet UITextField *passwordTF;
@property (strong, nonatomic) IBOutlet UITextField *phNumberTF;

@property (strong, nonatomic) IBOutlet UITextField *emailTF;

@property (strong, nonatomic) IBOutlet UITextField *firstName;
@end
