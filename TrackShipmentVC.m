
//  TrackShipmentVC.m
//  Dingo
//
//  Created by MAC on 06/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import "TrackShipmentVC.h"
#import "UserModal.h"
#import "WebService.h"
#import "UserModal.h"
#import "Annotation.h"
@interface TrackShipmentVC ()
{
    NSTimer*timer;
    CLGeocoder *geocoder;
}
@end

@implementation TrackShipmentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    geocoder=[[CLGeocoder alloc]init];
    // Do any additional setup after loading the view.
    [UserModal addShadowToView:_shadowView];
    _trackBtn.layer.cornerRadius=_trackBtn.frame.size.height/2;
    [UserModal addGradient:_trackBtn];
    if (_fromHome) {
        self.navigationItem.leftBarButtonItem=nil;

    }else{
        self.navigationItem.leftBarButtonItem=_leftBatBtn;

    }
    [self getLocation];
    timer=   [NSTimer scheduledTimerWithTimeInterval:6.0
                                              target:self
                                            selector:@selector(getLocation)
                                            userInfo:nil
                                             repeats:YES];
    if (_shipmentNumber) {
        _shipmentNumberTF.text=_shipmentNumber;

    }}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillDisappear:(BOOL)animated{
    [timer invalidate];
    timer=nil;

}
-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation{
    
    MKAnnotationView *annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CustomAnnotation"];
    annotationView.image = [UIImage imageNamed:@"address"];
    annotationView.draggable = YES;
    return annotationView;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)getLocation{
    if (_shipmentNumber==nil||[_shipmentNumber isEqualToString:@""]) {
           _shipmentNumber=_shipmentNumberTF.text;

    }

    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
                   {
                       // Background work
                       dispatch_async(dispatch_get_main_queue(), ^(void)
                                      {
                                          NSDictionary *dic=[[NSDictionary alloc]initWithObjectsAndKeys:[UserModal getUserId],@"user_id",_shipmentNumber,@"shipment_number",@"Get Tracker location",@"get_tracker_location", nil];
                                          [[WebService sharedInstance]parameters:dic onComplete:^(id responseObject) {
                                              if ([[responseObject valueForKey:@"status"]integerValue]==1) {
                                                  // [self mapLocationSet:[[CLLocation alloc] initWithLatitude:30.7046 longitude:76.7179]];
                                                  [self mapLocationSet:[[CLLocation alloc] initWithLatitude:[[responseObject valueForKeyPath:@"result.latitude"] doubleValue] longitude:[[responseObject valueForKeyPath:@"result.longitude"]  doubleValue]]];
                                              }else{
                                                  // [UserModal showAlert:@"Alert" withMessage:@"" withVC:self withPop:NO];
                                                  [timer invalidate];
                                              }
                                          } onError:^(NSError *error) {
                                              
                                          }];
                                      });
                   });
}
    - (IBAction)trackAction:(id)sender {
     NSString *shipm = [_shipmentNumberTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (shipm.length!=0) {
        timer=   [NSTimer scheduledTimerWithTimeInterval:6.0
                                                  target:self
                                                selector:@selector(getLocation)
                                                userInfo:nil
                                                 repeats:YES];
    }
}
-(void)mapLocationSet:(CLLocation*)locationn
{
    [_mapView removeAnnotations:_mapView.annotations];
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.005;
    span.longitudeDelta = 0.005;
    region.span = span;
    region.center =locationn.coordinate;
   
    [self.mapView setRegion:region animated:YES];

     [_mapView addAnnotation:[[Annotation alloc] initWithCoordinate:locationn.coordinate]];
    [self setAddress:locationn];
}
-(void)setAddress:(CLLocation*)location{
    
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
     {
         
         
         if (placemarks && placemarks.count > 0)
         {
             NSString *tempCity;
             CLPlacemark *placemark = placemarks[0];
             
             NSDictionary *addressDictionary =placemark.addressDictionary;
             
             NSArray*formattedAddressLines = [addressDictionary objectForKey:@"FormattedAddressLines"];
             NSString*    address = [formattedAddressLines objectAtIndex:0];
             if (formattedAddressLines.count>1) {
                 tempCity = [formattedAddressLines objectAtIndex:1];
             }
             
             NSArray* tempCityArray = [tempCity  componentsSeparatedByString:@","];
             
             //       NSString  *countryname_str=[addressDictionary objectForKey:@"Country"];
             
             _locLbl.text=[NSString stringWithFormat:@"%@,%@",address,[tempCityArray objectAtIndex:0]];
         }
     }];
    
}
@end
