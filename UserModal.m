//
//  UserModal.m
//  Verknow
//
//  Created by MAC on 05/04/17.
//  Copyright © 2017 MAC. All rights reserved.
//
#define KfirstName @"firstName"
#define KlastName @"lastName"
#define kprofilePic @"profilePic"
#define kemail @"email"
#define kphoneNumber @"PhoneNumber"
#define kUserId @"userId"
#define kuserType @"userType"

#import "UserModal.h"

@implementation UserModal

+ (instancetype)sharedInstance
{
    static UserModal *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[UserModal alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedInstance;
}
+(void)addShadow:(UIButton*)button{
    button.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    button.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    button.layer.shadowOpacity = 0.8;
    button.layer.shadowRadius = 8.0;

}
+(void)addShadowToView:(UIView*)button{
    button.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    button.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    button.layer.shadowOpacity = 0.8;
    button.layer.shadowRadius = 8.0;
    
}
+(void)saveUserName :(NSString *)firstName lastName:(NSString *)lastName profilePic:(NSString*)profilePic email:(NSString *)email  phNumber:(NSString*)phNumber userId:(NSString*)userId userType:(NSString*)userType {
    
    [[NSUserDefaults standardUserDefaults]setObject:firstName forKey:KfirstName];
    [[NSUserDefaults standardUserDefaults]setObject:lastName forKey:KlastName];
    [[NSUserDefaults standardUserDefaults]setObject:profilePic forKey:kprofilePic];
    [[NSUserDefaults standardUserDefaults]setObject:phNumber forKey:kphoneNumber];
    [[NSUserDefaults standardUserDefaults]setObject:email forKey:kemail];
    [[NSUserDefaults standardUserDefaults]setObject:userId forKey:kUserId];
    [[NSUserDefaults standardUserDefaults]setObject:userType forKey:kuserType];

    
}
+(NSString*)getFirstName{
    return [[NSUserDefaults standardUserDefaults]objectForKey:KfirstName];
    
}
+(NSString*)getLastname{
    return [[NSUserDefaults standardUserDefaults]objectForKey:KlastName];
    
}
+(NSString *)getProfilePic{
    
    return  [[NSUserDefaults standardUserDefaults]objectForKey:kprofilePic];
    
    
}
+(NSString *)getemail{
    return [[NSUserDefaults standardUserDefaults]objectForKey:kemail];
    
}

+(NSString *)getPhNumber{
    return [[NSUserDefaults standardUserDefaults]objectForKey:kphoneNumber];
    
}

+(NSString*)getUserId{
    return [[NSUserDefaults standardUserDefaults]objectForKey:kUserId];
}
+(NSString*)getUserType{
    return [[NSUserDefaults standardUserDefaults]objectForKey:kuserType];
}
+(void)showAlert:(NSString*)title withMessage:(NSString*)message withVC:(UIViewController*)VC withPop:(BOOL)pop{
    
UIAlertController *alertC = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    
    
    [alertC addAction:[UIAlertAction actionWithTitle:@"OK"  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        if (pop) {
            [VC.navigationController popViewControllerAnimated:YES];
        }
    }]];
    
    
    [VC presentViewController: alertC animated:YES completion:nil];

}
+(UIColor*)staticAppColor{
    return [UIColor colorWithRed:50/255.0 green:176/255.0 blue:255/255.0 alpha:0.5];
}
+(void)addLayer:(UITextField*)TF {
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 0.5;
    border.borderColor = [UIColor darkGrayColor].CGColor;
    border.frame = CGRectMake(0, TF.frame.size.height - borderWidth, TF.frame.size.width, TF.frame.size.height);
    border.borderWidth = borderWidth;
    [TF.layer addSublayer:border];
    TF.layer.masksToBounds=YES;
}
+(void)addGradient:(UIButton*)type{
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = type.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[UIColor colorWithRed:230/255.0 green:228/255.0 blue:228/255.0 alpha:0.5].CGColor, (id)[UIColor colorWithRed:50/255.0 green:176/255.0 blue:255/255.0 alpha:0.5].CGColor, nil];
    gradient.startPoint = CGPointZero;
    gradient.endPoint = CGPointMake(1, 1);
    gradient.cornerRadius = type.layer.cornerRadius;

    [type.layer insertSublayer:gradient atIndex:0];
}
+(void)addGradientToView:(UIView*)type{
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = type.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[UIColor colorWithRed:230/255.0 green:228/255.0 blue:228/255.0 alpha:0.5].CGColor, (id)[UIColor colorWithRed:50/255.0 green:176/255.0 blue:255/255.0 alpha:0.5].CGColor, nil];
    gradient.startPoint = CGPointZero;
    gradient.endPoint = CGPointMake(1, 1);
    gradient.cornerRadius = type.layer.cornerRadius;
    
    [type.layer insertSublayer:gradient atIndex:0];
}
+(void)addBorder:(UIButton*)btn color:(UIColor*)color{
    btn.layer.borderColor=color.CGColor;
    btn.layer.borderWidth=1.0;
}

@end
