//
//  UploadBillsVC.m
//  Dingo
//
//  Created by MAC on 06/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import "UploadBillsVC.h"
#import "UserModal.h"
#import "WebService.h"
#import "SVProgressHUD.h"
@interface UploadBillsVC ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    UIImage *billImage;
    NSString *uploadType;
}
@end

@implementation UploadBillsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _notifyBtn.layer.cornerRadius=_notifyBtn.frame.size.height/2;
    if (_from) {
        self.navigationItem.leftBarButtonItem=nil;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillLayoutSubviews{
    [UserModal addGradient:_notifyBtn];
    _uploadBillsBtn.layer.borderColor=[UIColor darkGrayColor].CGColor;
    [UserModal addBorder:_uploadBillsBtn color:[UIColor colorWithRed:230/255.0 green:228/255.0 blue:228/255.0 alpha:1]];
    [UserModal addBorder:_signedRecieptBtn color:[UIColor colorWithRed:230/255.0 green:228/255.0 blue:228/255.0 alpha:1]];
    [UserModal addBorder:_cancelBtn color:[UserModal staticAppColor]];
    _cancelBtn.layer.cornerRadius=_notifyBtn.frame.size.height/2;
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)uploadBillsAction:(id)sender {
    uploadType=@"1";
    [self uploadPic];
}

- (IBAction)uploadReceiptAction:(id)sender {
    uploadType=@"2";

    [self uploadPic];
}
- (IBAction)notifyAction:(id)sender {
}
- (IBAction)cancelAction:(id)sender {
}
-(void)uploadPic{
    UIAlertController *alertC = [UIAlertController alertControllerWithTitle:@"Select Method" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    [alertC addAction:[UIAlertAction actionWithTitle:@"Camera"  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePickerController.delegate = self;
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }]];
    
    [alertC addAction:[UIAlertAction actionWithTitle:@"Gallery"  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePickerController.delegate = self;
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }]];
    [alertC addAction:[UIAlertAction actionWithTitle:@"Cancel"  style:UIAlertActionStyleCancel handler:^(UIAlertAction * action){
        
    }]];
    
    [self presentViewController: alertC animated:YES completion:nil];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info;
{
   billImage = [info valueForKey:UIImagePickerControllerOriginalImage];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}
#pragma mark- webServices
-(void)uploadBills{
    [SVProgressHUD show];
    
    NSDictionary *dic=[[NSDictionary alloc]initWithObjectsAndKeys:[UserModal getUserId],@"user_id",@"",@"shipment_id",uploadType,@"upload_type", nil];
    [[WebService sharedInstance]parameters:dic image:UIImageJPEGRepresentation(billImage, 0.5) imageparameter:@"bill_image" onComplete:^(id responseObject) {
        [SVProgressHUD dismiss];
        if([[responseObject valueForKey:@"status"]integerValue]==1){
        
        }
        
    } onError:^(NSError *error) {
        [SVProgressHUD dismiss];

    }];
}
@end
