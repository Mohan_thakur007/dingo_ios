//
//  SettingVC.m
//  Dingo
//
//  Created by MAC on 06/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import "SettingVC.h"
#import "UserModal.h"
@interface SettingVC ()
{
    NSArray *items,*imagesArr;
}
@end

@implementation SettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    items=@[@"My Account",@"History",@"Help",@"Logout"];
    imagesArr=@[@"user_icon",@"history",@"help",@"logout_blue"];
}
-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden=YES;

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillLayoutSubviews{
    _profileBtn.layer.cornerRadius=_profileBtn.frame.size.height/2;
    [UserModal addGradientToView:_shadowview];
    [UserModal addShadow:_profileBtn];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark- UITableView Datasource


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    [(UILabel*)[cell viewWithTag:5] setText:[items objectAtIndex:indexPath.row]];
    [(UIImageView*)[cell viewWithTag:52]setImage:[UIImage imageNamed:[imagesArr objectAtIndex:indexPath.row]]];
    UILabel *lbl=(UILabel*)[cell viewWithTag:10];
    if (indexPath.row==1||indexPath.row==2) {
        lbl.hidden=NO;
    }else{
        lbl.hidden=YES;
    }
    return cell;
}
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row==0) {
        UIViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileVC"];
        [self.navigationController pushViewController:vc animated:YES];
    }
    if (indexPath.row==3) {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil message:@"Are you sure you want to logout?" preferredStyle:UIAlertControllerStyleActionSheet];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"Logout" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action){
            [self resetDefaults];
            UIViewController *homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
            
            UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:homeViewController];
            
            [UIView transitionWithView:[UIApplication sharedApplication].keyWindow
                              duration:0.5
                               options:UIViewAnimationOptionTransitionFlipFromLeft
                            animations:^{ [UIApplication sharedApplication].keyWindow.rootViewController=nav; }
                            completion:nil];
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action){
            
        }]];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (IBAction)profileAction:(id)sender {
    UIViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileVC"];
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)resetDefaults {
    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
    NSDictionary * dict = [defs dictionaryRepresentation];
    for (id key in dict) {
        if ([key isEqualToString:@"email1"]||[key isEqualToString:@"pass"]) {
            
        }else{
            [defs removeObjectForKey:key];
            
        }
    }
    [defs synchronize];
}

@end
