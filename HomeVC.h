//
//  HomeVC.h
//  Dingo
//
//  Created by MAC on 05/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@interface HomeVC : UIViewController
- (IBAction)createShipmentAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *createShipmentBtn;

@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@end
