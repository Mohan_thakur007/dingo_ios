//
//  MenuVC.h
//  Dingo
//
//  Created by MAC on 05/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuVC : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *profilePic;

@property (strong, nonatomic) IBOutlet UILabel *userNameLbl;
@end
