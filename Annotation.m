//
//  Annotation.m
//  Dingo
//
//  Created by MAC on 08/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import "Annotation.h"

@implementation Annotation
@synthesize coordinate;
- (id) initWithCoordinate:(CLLocationCoordinate2D)coord
{
    [UIView animateWithDuration:5 animations:^{
        coordinate = coord;
    }];
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
