//
//  ViewController.m
//  Dingo
//
//  Created by MAC on 03/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import "LoginViewController.h"
#import "UserModal.h"
#import <GoogleSignIn/GoogleSignIn.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "AFNetworking.h"
#import "WebService.h"
#import "SVProgressHUD.h"
#import "SocialMediaDetailVC.h"
#import "DashboardVC.h"
@interface LoginViewController ()<GIDSignInDelegate,GIDSignInUIDelegate>

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _loginBtn.layer.cornerRadius=_loginBtn.frame.size.height/2;
    _createBtn.layer.cornerRadius=10;
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden=YES;
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"email1"]) {
        _emailTF.text=[[NSUserDefaults standardUserDefaults]objectForKey:@"email1"];
        _passwordTF.text=[[NSUserDefaults standardUserDefaults]objectForKey:@"pass"];
        [_remmemberBtn setSelected:YES];
        [_remmemberBtn setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
    }
    
}

-(void)viewWillLayoutSubviews{

    [UserModal addLayer:_emailTF];
    [UserModal addLayer:_passwordTF];
    [UserModal addGradient:_createBtn];
    [UserModal addGradient:_loginBtn];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)loginAction:(id)sender {
    if ([_remmemberBtn isSelected]) {
        [[NSUserDefaults standardUserDefaults]setObject:_emailTF.text forKey:@"email1"];
        [[NSUserDefaults standardUserDefaults]setObject:_passwordTF.text forKey:@"pass"];
    }else{
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"email1"];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"pass"];
    }
    if([self validateFields]){
        [self login];
    }
  }

- (IBAction)loginWithFBAction:(id)sender {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    [login
     logInWithReadPermissions: @[@"public_profile", @"email", @"user_friends"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error");
             
         }
         else if (result.isCancelled)
         {
             NSLog(@"Cancelled");
             
         }
         else
         {
           //  [SVProgressHUD show];
             
             NSLog(@"Logged in %@ %@", result.token.userID, result.token.tokenString);
             [self fetchUserInfo];
             
         }
     }];

}
-(void)fetchUserInfo
{
    if ([FBSDKAccessToken currentAccessToken])
    {
        NSLog(@"Token is available : %@",[[FBSDKAccessToken currentAccessToken]tokenString]);
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, link, first_name, last_name, picture.type(large), email,gender, birthday,work ,location ,friends ,hometown , friendlists"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error)
             {
                 NSLog(@"resultis:%@",result);
                 
                 [self checkUserExistence:@"1" withID:[NSString stringWithFormat:@"%@",[result valueForKey:@"id"]]fname:[result valueForKey:@"first_name"] lname:[result valueForKey:@"last_name"] email:[result valueForKey:@"email"] image:[result valueForKeyPath:@"picture.data.url"]];
                 
//                 SocialMediaDetailVC *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"SocialMediaDetailVC"];
//                 vc.userDetailDic=@{@"access_token":[NSString stringWithFormat:@"%@",[result valueForKey:@"id"]],@"first_name":[result valueForKey:@"first_name"],@"last_name":[result valueForKey:@"last_name"],@"user_name":[NSString stringWithFormat:@"%@ %@",[result valueForKey:@"first_name"],[result valueForKey:@"last_name"]],@"email":[result valueForKey:@"email"],@"user_image":[result valueForKeyPath:@"picture.data.url"],@"login_type":@"1"};
//                 [self.navigationController pushViewController:vc animated:YES];

             }
             else
             {
                 NSLog(@"Error %@",error);
               //  [SVProgressHUD dismiss];
             }
         }];
    }
    
}
- (IBAction)loginWIthGooglePlusAction:(id)sender {
    [[GIDSignIn sharedInstance] signOut];
    [GIDSignIn sharedInstance].uiDelegate = self;
    [GIDSignIn sharedInstance].delegate=self;
    [GIDSignIn sharedInstance].shouldFetchBasicProfile = YES;

    [[GIDSignIn sharedInstance] signIn];
}
- (IBAction)forgotAction:(id)sender {
    [self performSegueWithIdentifier:@"forgot" sender:nil];
}
- (IBAction)resgisterAction:(id)sender {
    [self performSegueWithIdentifier:@"signUp" sender:nil];
}

- (IBAction)remmeberAction:(id)sender {
    if (![_remmemberBtn isSelected]) {
        [sender setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
        [_remmemberBtn setSelected:YES];
        
    }else{
        [_remmemberBtn setSelected:NO];
        [sender setImage:[UIImage imageNamed:@"uncheck"] forState:UIControlStateNormal];
        
    }

}
#pragma mark- google login
- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController {
    
    [self presentViewController:viewController animated:YES completion:nil];
}
- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations on signed in user here.
    NSString *userId = user.userID;                  // For client-side use only!
  //  NSString *idToken = user.authentication.idToken; // Safe to send to the server
    NSString *name = user.profile.name;
    NSArray *items = [name componentsSeparatedByString:@" "];   //take one array for splitting the string
    NSString *fname = [items objectAtIndex:0];
    NSString *lname = [items objectAtIndex:1];
    NSString *email = user.profile.email;
       NSString *profilePIctureUrl=[user.profile imageURLWithDimension:150*150].absoluteString;
    [self checkUserExistence:@"2" withID:userId fname:fname lname:lname email:email image:profilePIctureUrl];

   

}
- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations when the user disconnects from app here.
    // ...
 //   [SVProgressHUD showInfoWithStatus:@"Login Cancelled"];
    
}
#pragma mark- validate fields
-(BOOL)validateFields{
    NSString *emailId = [_emailTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *password = [_passwordTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString *emailRegEx = @"[0-9a-zA-Z._%+-]+@[a-z0-9A_Z.-]+\\.[A_Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    
    if (emailId.length==0||password.length==0) {
        [UserModal showAlert:@"Alert" withMessage:@"Please fill empty feilds" withVC:self withPop:NO];
        return NO;
    }
    if([emailTest evaluateWithObject:emailId] != YES && [emailId length]!=0){
        [UserModal showAlert:@"Alert" withMessage:@"Please Enter valid email" withVC:self withPop:NO];
        return NO;
        
    }    return YES;
}
#pragma mark- webServices
-(void)login{
    [SVProgressHUD show];
    NSDictionary *dic=[[NSDictionary alloc]initWithObjectsAndKeys:_emailTF.text,@"email",_passwordTF.text,@"password",@"",@"device_id",@"2",@"device_type",@"Login",@"login" ,nil];
    [[WebService sharedInstance]parameters:dic onComplete:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([[responseObject valueForKey:@"status"] integerValue]==1) {
            [UserModal saveUserName:[responseObject valueForKeyPath:@"result.first_name"] lastName:[responseObject valueForKeyPath:@"result.last_name"] profilePic:[NSString stringWithFormat:@"%@%@",[responseObject valueForKey:@"url"],[responseObject valueForKeyPath:@"result.user_image"]] email:[responseObject valueForKeyPath:@"result.email"] phNumber:[responseObject valueForKeyPath:@"result.phone_number"] userId:[responseObject valueForKeyPath:@"result.id"]userType:[responseObject valueForKeyPath:@"result.user_type"]];
            
            [self performSegueWithIdentifier:@"home" sender:nil];
        // DashboardVC *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"DashboardVC"];
        //        [self.navigationController pushViewController:vc animated:YES];


        }else{
             [UserModal showAlert:@"Alert" withMessage:@"User doesn't exist" withVC:self withPop:NO];
        }
    } onError:^(NSError *error) {
        [SVProgressHUD dismiss];
        [UserModal showAlert:@"Error!!" withMessage:error.localizedDescription withVC:self withPop:NO];

    }];
}
-(void)checkUserExistence:(NSString *)logintype withID:(NSString*)uId fname:(NSString*)fname lname:(NSString*)lname email:(NSString*)email image:(NSString*)image{
    [SVProgressHUD show];
    NSDictionary *dic=[[NSDictionary alloc]initWithObjectsAndKeys:uId,@"access_token",logintype,@"login_type",@"Check Register",@"check_register", nil];
    [[WebService sharedInstance]parameters:dic onComplete:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([[responseObject valueForKey:@"status"]integerValue]==1) {
            [UserModal saveUserName:[responseObject valueForKeyPath:@"result.first_name"] lastName:[responseObject valueForKeyPath:@"result.last_name"] profilePic:[responseObject valueForKeyPath:@"result.user_image"] email:[responseObject valueForKeyPath:@"result.email"] phNumber:[responseObject valueForKeyPath:@"result.phone_number"] userId:[responseObject valueForKeyPath:@"result.user_id"]userType:[responseObject valueForKeyPath:@"result.user_type"]];
            [self performSegueWithIdentifier:@"home" sender:nil];

        }else{
            
            SocialMediaDetailVC *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"SocialMediaDetailVC"];
            vc.userDetailDic=@{@"access_token":uId,@"first_name":fname,@"last_name":lname,@"user_name":[NSString stringWithFormat:@"%@ %@",fname,lname],@"email":email,@"user_image":image,@"login_type":logintype};
            [self.navigationController pushViewController:vc animated:YES];
            
            
        }
    } onError:^(NSError *error) {
        [SVProgressHUD dismiss];

    }];
}
@end
