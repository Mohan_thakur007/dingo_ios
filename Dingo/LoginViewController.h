//
//  ViewController.h
//  Dingo
//
//  Created by MAC on 03/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *loginBtn;
- (IBAction)loginAction:(id)sender;
- (IBAction)loginWithFBAction:(id)sender;
- (IBAction)loginWIthGooglePlusAction:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *emailTF;
@property (strong, nonatomic) IBOutlet UITextField *passwordTF;
- (IBAction)forgotAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *createBtn;
- (IBAction)resgisterAction:(id)sender;
- (IBAction)remmeberAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *remmemberBtn;

@end

