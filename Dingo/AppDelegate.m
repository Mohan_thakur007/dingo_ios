//
//  AppDelegate.m
//  Dingo
//
//  Created by MAC on 03/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import "AppDelegate.h"
#import "UserModal.h"
#import <Google/SignIn.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "WebService.h"
@import GooglePlaces;
@interface AppDelegate ()<UIApplicationDelegate>
{
     CLLocationManager *locationManager;
}
@end

@implementation AppDelegate
static NSString * const kClientID =@"80730290119-8f3fvc7c5ef44651iuqhlk3pvpknnb6f.apps.googleusercontent.com";


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [self naviagtionAppearance];
    if ([UserModal getUserId]) {
        [self getTreuckerCurrentStatus];
        UIStoryboard *sb=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *homeViewController = [sb instantiateViewControllerWithIdentifier:@"SideMenuVC"];
        self.window.rootViewController=homeViewController;
    }
    locationManager = [[CLLocationManager alloc] init];
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [locationManager requestWhenInUseAuthorization];
    }
    [GMSPlacesClient provideAPIKey:@"AIzaSyCFPNYLXokmOheCbuCyO5uDPK2CphhNML0"];
    [GIDSignIn sharedInstance].clientID = kClientID;
        return YES;
}

-(void)naviagtionAppearance{
    
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60) forBarMetrics:UIBarMetricsDefault];
    [UINavigationBar appearance].translucent=NO;
    [[UINavigationBar appearance]setBarTintColor:[UserModal staticAppColor]];
    [[UINavigationBar appearance]setTintColor:[UIColor whiteColor]];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = CGRectMake(0, 0, self.window.frame.size.width, 80);
    gradient.colors = [NSArray arrayWithObjects:(id)[UIColor colorWithRed:230/255.0 green:228/255.0 blue:228/255.0 alpha:1.0].CGColor, (id)[UIColor colorWithRed:58/255.0 green:191/255.0 blue:255/255.0 alpha:0.7].CGColor, nil];
    gradient.startPoint = CGPointZero;
    gradient.endPoint = CGPointMake(1, 1);
    [[UINavigationBar appearance]setBackgroundImage:[self imageFromLayer:gradient] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];

}
- (UIImage *)imageFromLayer:(CALayer *)layer
{
    UIGraphicsBeginImageContext([layer frame].size);
    
    [layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return outputImage;
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    if ([[url scheme] isEqualToString:@"fb236522483527456"]) {
        
        return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                              openURL:url
                                                    sourceApplication:sourceApplication
                                                           annotation:annotation];
    }else{
        return [[GIDSignIn sharedInstance] handleURL:url
                                   sourceApplication:sourceApplication
                                          annotation:annotation];
    }
}
-(void)getTreuckerCurrentStatus{
    if ([[UserModal getUserType] integerValue]==2){
        [[WebService sharedInstance]parameters:@{@"tracker_id":[UserModal getUserId],@"get_trucker_status":@"Get Trucker Status"} onComplete:^(id responseObject) {
            if ([[responseObject valueForKey:@"status"]integerValue]==1) {
                [[UserModal sharedInstance]setTruckerStatusWithResposne:[responseObject valueForKey:@"result"]];
                [[NSNotificationCenter defaultCenter]postNotificationName:@"getStatus"object:self];

            }else{
                [[UserModal sharedInstance]setTruckerStatusWithResposne:nil];
            }
        } onError:^(NSError *error) {
            [[UserModal sharedInstance]setTruckerStatusWithResposne:nil];

        }];
    }

}

@end
