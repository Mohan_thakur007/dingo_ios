//
//  ShipmentCell.h
//  Dingo
//
//  Created by MAC on 05/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShipmentCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIView *cellView;
@property (weak, nonatomic) IBOutlet UIButton *trackBtn;
@property (weak, nonatomic) IBOutlet UIButton *detailBtn;
@property (weak, nonatomic) IBOutlet UIButton *statusBtn;

@property (strong, nonatomic) IBOutlet UILabel *shipmentName;

@end
