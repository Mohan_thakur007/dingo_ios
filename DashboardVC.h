//
//  DashboardVC.h
//  Dingo
//
//  Created by MAC on 10/12/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashboardVC : UIViewController

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *upperCollectionView;


@end
