//
//  SettingVC.h
//  Dingo
//
//  Created by MAC on 06/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingVC : UIViewController
@property (strong, nonatomic) IBOutlet UIView *shadowview;
@property (strong, nonatomic) IBOutlet UIView *aboutView;
- (IBAction)profileAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *profileBtn;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@end
