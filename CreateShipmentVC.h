//
//  CreateShipmentVC.h
//  Dingo
//
//  Created by MAC on 05/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateShipmentVC : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *fromBtn;
@property (strong, nonatomic) IBOutlet UIView *pickUpView;
@property (strong, nonatomic) IBOutlet UIButton *toBtn;
@property (strong, nonatomic) IBOutlet UIView *sizeView;
@property (strong, nonatomic) IBOutlet UIButton *submitBtn;
@property (strong, nonatomic) IBOutlet UIButton *addShipmentBtn;
@property (strong, nonatomic) IBOutlet UIView *weightView;
@property (strong, nonatomic) IBOutlet UIView *shippmentDetailView;

- (IBAction)addShipmentAction:(id)sender;
- (IBAction)submitAction:(id)sender;
- (IBAction)dropOffAction:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *pickWIndowTF;
@property (strong, nonatomic) IBOutlet UITextField *loadSizeTF;
@property (strong, nonatomic) IBOutlet UITextField *loadWeightTF;
@property (strong, nonatomic) IBOutlet UITextField *shipmntName;



- (IBAction)pickUpAction:(id)sender;
@end
