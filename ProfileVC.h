//
//  ProfileVC.h
//  Dingo
//
//  Created by MAC on 03/07/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileVC : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *profilePic;
@property (strong, nonatomic) IBOutlet UITextField *firstName;
- (IBAction)submitAction:(id)sender;
- (IBAction)cameraAction:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *submitBtn;
@property (strong, nonatomic) IBOutlet UITextField *lastName;
- (IBAction)editAction:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *phNumberTF;

@end
