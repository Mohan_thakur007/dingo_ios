//
//  OnRouteShipmentVC.h
//  Dingo
//
//  Created by mac on 7/13/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "SelectShipmentVC.h"

static NSString *kSavedItemsKey = @"savedItems";

@interface OnRouteShipmentVC : UIViewController
- (IBAction)haultAction:(id)sender;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIView *detailView;
@property (weak, nonatomic) IBOutlet UITextView *haltTxtView;
- (IBAction)closeAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@property (weak, nonatomic) IBOutlet UIButton *completeBtn;
- (IBAction)completeShipmentAction:(id)sender;
@property NSString *shipmentNumber;
@property NSString *shimpmentID;
@property (weak, nonatomic) IBOutlet UIButton *haultBtn;
@property (weak, nonatomic) IBOutlet UIView *haltView;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UILabel *fromLabel;
@property (weak, nonatomic) IBOutlet UILabel *toLabel;
@property (strong, nonatomic) IBOutlet UIVisualEffectView *blurView;
@property (weak, nonatomic) IBOutlet UILabel *reasonLbl;
- (IBAction)submitHaltAction:(id)sender;
@property NSArray *shipmentDetail;
@property NSInteger shipmentStatus;






@end
