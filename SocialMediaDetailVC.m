//
//  SocialMediaDetailVC.m
//  Dingo
//
//  Created by MAC on 30/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import "SocialMediaDetailVC.h"
#import "UserModal.h"
#import "SVProgressHUD.h"
#import "WebService.h"
@interface SocialMediaDetailVC ()<UIPickerViewDelegate,UIPickerViewDataSource>
{
    UIPickerView *pickerView1;
    NSArray *items;

}
@end

@implementation SocialMediaDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    pickerView1=[[UIPickerView alloc]initWithFrame:CGRectMake(0, 40, self.view.frame.size.width, 200)];
    pickerView1.delegate=self;
    pickerView1.dataSource=self;
    

    items=@[@"Shipper",@"Trucker"];
}
-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden=NO;
}
-(void)viewWillLayoutSubviews{
    for (UITextField *TF in [self.view subviews]) {
        if ([TF isKindOfClass:[UITextField class]]) {
            if (TF!=_typeOfUser) {
                [UserModal addLayer:TF];
            }
            
        }
    }
    _submitBtn.layer.cornerRadius=_submitBtn.frame.size.height/2;
    [UserModal addGradient:_submitBtn];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField==_typeOfUser) {
        _typeOfUser.inputView=pickerView1;
        [pickerView1 reloadAllComponents];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - pickerView DataSouce&Delegates
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return items.count;
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    [self pickerView:pickerView didSelectRow:0 inComponent:0];

    return [items objectAtIndex:row];
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    self.typeOfUser.text=[items objectAtIndex:row];
    
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 40;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)submitAction:(id)sender {
    if ([self validateFields]) {
        [self socialMediaLogin];
    }
}
#pragma mark- validate fields
-(BOOL)validateFields{
      NSString *buisnessName = [_buisnessNameTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *mcNumber = [_mcNumberTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *dotNumber = [_dotNumberTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *poc = [_pocTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *phoneNumber = [_phNumberTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *userType = [_typeOfUser.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    
    
    if (buisnessName.length==0||mcNumber.length==0||dotNumber.length==0||poc.length==0||phoneNumber.length==0||userType.length==0) {
        [UserModal showAlert:@"Alert" withMessage:@"Please fill empty feilds" withVC:self withPop:NO];
        return NO;
    }
       if (phoneNumber.length<10) {
        [UserModal showAlert:@"Alert" withMessage:@"Please Enter valid contact number" withVC:self withPop:NO];
        
        return NO;
    }
   
    return YES;
}
-(void)socialMediaLogin{
    NSString *type;
    if ([_typeOfUser.text isEqualToString:@"Trucker"]) {
        type=@"2";
    }else{
        type=@"1";
    }
    [SVProgressHUD show];
    NSMutableDictionary *dic=[[NSMutableDictionary alloc]initWithObjectsAndKeys:_buisnessNameTF.text,@"business_name",_mcNumberTF.text,@"mc_number",_pocTF.text,@"poc",_dotNumberTF.text,@"dot_number",_phNumberTF.text,@"phone_number",type,@"user_type",@"Facebook API",@"facebook_api", nil];
    [dic addEntriesFromDictionary:_userDetailDic];
    [[WebService sharedInstance]parameters:dic onComplete:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([[responseObject valueForKey:@"status"] integerValue]==1) {
            
            [UserModal saveUserName:[responseObject valueForKeyPath:@"result.first_name"] lastName:[responseObject valueForKeyPath:@"result.last_name"] profilePic:[responseObject valueForKeyPath:@"result.user_image"] email:[responseObject valueForKeyPath:@"result.email"] phNumber:[responseObject valueForKeyPath:@"result.phone_number"] userId:[responseObject valueForKeyPath:@"result.user_id"]userType:[responseObject valueForKeyPath:@"result.user_type"]];
            [self performSegueWithIdentifier:@"login" sender:nil];
        }else{
            
        }
    } onError:^(NSError *error) {
        [SVProgressHUD dismiss];
        [UserModal showAlert:@"Error!!" withMessage:error.localizedDescription withVC:self withPop:NO];
    }];
}
@end
