//
//  ShipmentPriceVC.h
//  Dingo
//
//  Created by MAC on 06/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShipmentPriceVC : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *submitBtn;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)submitAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *acceptbtn;
- (IBAction)acceptTermsCondtionsAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *ownPriceTF;

- (IBAction)selectedPriceAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (weak, nonatomic) IBOutlet UITextField *shipmentPrice;
@property NSString *estimatedCost;
@property NSDictionary *postDic;
@end
