//
//  UserModal.h
//  Verknow
//
//  Created by MAC on 05/04/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface UserModal : NSObject
+ (instancetype)sharedInstance;
+(void)addShadow:(UIButton*)button;
+(void)showAlert:(NSString*)title withMessage:(NSString*)message withVC:(UIViewController*)VC withPop:(BOOL)pop;
+(UIColor*)staticAppColor;
+(NSString*)getFirstName;
+(NSString *)getemail;
+(NSString *)getProfilePic;
+(NSString*)getLastname;
+(NSString *)getPhNumber;
+(NSString*)getUserId;
+(void)addLayer:(UITextField*)TF;
+(void)addGradient:(UIButton*)type;
+(void)addShadowToView:(UIView*)button;
+(void)addGradientToView:(UIView*)type;
+(void)addBorder:(UIButton*)btn color:(UIColor*)color;
+(void)saveUserName :(NSString *)firstName lastName:(NSString *)lastName profilePic:(NSString*)profilePic email:(NSString *)email  phNumber:(NSString*)phNumber userId:(NSString*)userId userType:(NSString*)userType;
@property NSString *userType;
@property NSArray *truckerStatusWithResposne;
+(NSString*)getUserType;
@end
