//
//  ProfileVC.m
//  Dingo
//
//  Created by MAC on 03/07/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import "ProfileVC.h"
#import "UserModal.h"
#import "SVProgressHUD.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "WebService.h"
@interface ProfileVC ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@end

@implementation ProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _submitBtn.enabled=NO;
    _submitBtn.alpha=0.5;
    [self.view setUserInteractionEnabled:NO];
    [_profilePic sd_setImageWithURL:[NSURL URLWithString:[UserModal getProfilePic]]placeholderImage:[UIImage imageNamed:@"defaultpic"]];
    _profilePic.layer.cornerRadius=self.view.frame.size.height*0.07;
    _profilePic.clipsToBounds=YES;
    _firstName.text=[UserModal getFirstName];
    _lastName.text=[UserModal getLastname];
    _phNumberTF.text=[UserModal getPhNumber];
    
    _profilePic.layer.cornerRadius=self.view.frame.size.height*0.07;
    _profilePic.clipsToBounds=YES;
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandlerMethod:)];
    tapRecognizer.numberOfTapsRequired=1;
    _profilePic.userInteractionEnabled=YES;
    [_profilePic addGestureRecognizer:tapRecognizer];
}
-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden=NO;
    
}
-(void)viewWillLayoutSubviews{
    for (UITextField *TF in [self.view subviews]) {
        if ([TF isKindOfClass:[UITextField class]]) {
                [UserModal addLayer:TF];
            
        }
    }
    _submitBtn.layer.cornerRadius=_submitBtn.frame.size.height/2;
    [UserModal addGradient:_submitBtn];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
       if (![_firstName.text isEqual:[UserModal getFirstName]] ||![_lastName.text isEqual:[UserModal getLastname]]||![_phNumberTF.text isEqual:[UserModal getPhNumber]]) {
        _submitBtn.enabled=YES;
        _submitBtn.alpha=1.0;
    }else{
        _submitBtn.enabled=NO;
        _submitBtn.alpha=0.5;
    }
    
}
-(BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (textField==_phNumberTF) {
        if(text.length > 14) {
            return NO;
            
        }
        
        
    }
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)submitAction:(id)sender {
    if ([self validateFeilds]) {
        [self editProfile];
    }
       // [self.view setUserInteractionEnabled:NO];

}

- (IBAction)cameraAction:(id)sender {
    [self imagePicker];
}
- (IBAction)editAction:(id)sender {
    if ([sender isSelected]) {
        [self.view setUserInteractionEnabled:NO];
        [sender setSelected:NO];
        _submitBtn.hidden=YES;


    }else{
        [self.view setUserInteractionEnabled:YES];
        [sender setSelected:YES];
        _submitBtn.hidden=NO;

    }

}
#pragma mark- validation
-(BOOL)validateFeilds{
    NSString *firstName = [_firstName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *lastName = [_lastName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *phoneNumber = [_phNumberTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (firstName.length==0||lastName.length==0||phoneNumber.length==0) {
        [UserModal showAlert:@"Alert" withMessage:@"Please fill empty feilds" withVC:self withPop:NO];
        return NO;
    }
    if (phoneNumber.length<10) {
        [UserModal showAlert:@"Alert" withMessage:@"Please Enter valid contact number" withVC:self withPop:NO];
        
        return NO;
    }
    return YES;
}
#pragma mark- webservice
-(void)editProfile{
    [SVProgressHUD show];
    NSDictionary *dic=[[NSDictionary alloc]initWithObjectsAndKeys:[UserModal getUserId],@"user_id",_firstName.text,@"first_name",_lastName.text,@"last_name",_phNumberTF.text,@"phone_number",@"Edit Profile",@"edit_profile", nil];
    [[WebService sharedInstance]parameters:dic image:UIImageJPEGRepresentation(_profilePic.image, 0.5) imageparameter:@"user_image" onComplete:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([[responseObject valueForKey:@"status"]integerValue]==1) {
            [[NSUserDefaults standardUserDefaults]setObject:[responseObject valueForKeyPath:@"result.first_name"]
                                                     forKey:@"firstName"];
            [[NSUserDefaults standardUserDefaults]setObject:[responseObject valueForKeyPath:@"result.last_name"]
                                                     forKey:@"lastName"];
            [[NSUserDefaults standardUserDefaults]setObject:[responseObject valueForKeyPath:@"result.phone_number"]
                                                     forKey:@"PhoneNumber"];
        //    [[NSUserDefaults standardUserDefaults]setObject:[responseObject valueForKeyPath:@"result.url"]
                //                                     forKey:@""];

            [UserModal showAlert:@"Message" withMessage:@"Update profile Successfully" withVC:self withPop:NO];
            _submitBtn.alpha=0.5;
            _submitBtn.enabled=NO;
        }
    } onError:^(NSError *error) {
        [UserModal showAlert:@"Error!!" withMessage:error.localizedDescription withVC:self withPop:NO];

    }];
}
-(void)gestureHandlerMethod:(UITapGestureRecognizer*)sender {
    [self imagePicker];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info;
{
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    _profilePic.image=image;
    _submitBtn.enabled=YES;
    _submitBtn.alpha=1.0;
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}
-(void)imagePicker{
    UIAlertController *alertC = [UIAlertController alertControllerWithTitle:@"Select Method" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    [alertC addAction:[UIAlertAction actionWithTitle:@"Camera"  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePickerController.delegate = self;
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }]];
    
    [alertC addAction:[UIAlertAction actionWithTitle:@"Gallery"  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePickerController.delegate = self;
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }]];
    [alertC addAction:[UIAlertAction actionWithTitle:@"Cancel"  style:UIAlertActionStyleCancel handler:^(UIAlertAction * action){
        
    }]];
    
    [self presentViewController: alertC animated:YES completion:nil];
}

@end
