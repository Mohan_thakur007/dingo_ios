//
//  WebService.m
//  SingletonExample
//


#import "WebService.h"
#define webServiceLink @"http://brightdeveloper.net/dingo/index.php/Webservice"
@implementation WebService


+ (instancetype)sharedInstance
{
    static WebService *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[WebService alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedInstance;
}

-(void)parameters:(NSDictionary *)paramters  onComplete:(void (^)(id responseObject))successBlock
   onError:(void (^)(NSError *error))errorBlock
{
    
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
     manager.responseSerializer.acceptableContentTypes=[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",nil];
 
    [manager POST:webServiceLink parameters:paramters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
          successBlock(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
       ;
        NSLog(@"%@", task.response);
         errorBlock(error);
    }];
    
    
}

-(void) parameters:(NSDictionary *)paramters image:(NSData*)image imageparameter:(NSString*)imageparamter onComplete:(void (^)(id responseObject))successBlock
   onError:(void (^)(NSError *error))errorBlock
{
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];

    [manager POST:webServiceLink parameters:paramters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:image
                                    name:imageparamter
                                fileName:@"abc" mimeType:@"image/jpeg"];
        
    } progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        successBlock(responseObject);

    } failure:^(NSURLSessionDataTask *task, NSError *error) {
           errorBlock(error);
    }];
 
}


@end
