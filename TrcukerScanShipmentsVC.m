//
//  TrcukerScanShipmentsVC.m
//  Dingo
//
//  Created by MAC on 06/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import "TrcukerScanShipmentsVC.h"
#import "UserModal.h"
#import "SVProgressHUD.h"
#import "WebService.h"
#import "SelectShipmentVC.h"
#import "OnRouteShipmentVC.h"
@import GooglePlacePicker;
@interface TrcukerScanShipmentsVC ()<GMSAutocompleteViewControllerDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
    UIDatePicker *datePicker;
    NSDateFormatter *dateFormat;
    UIPickerView *pickerView1;
    NSArray *items;
    BOOL pickup;
    NSString *pickUpLat,*pickupLongitute,*dropoffLat,*dropoffLongitute;
    NSMutableArray *shipmentsArr;
    CLGeocoder *geocoder;
    CLLocation *newLocation;
}

@end

@implementation TrcukerScanShipmentsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    geocoder=[[CLGeocoder alloc]init];
   shipmentsArr=[[NSMutableArray alloc]init];

    [self setUpDatePicker];
    // Do any additional setup after loading the view.
    _scanBtn.layer.cornerRadius=_scanBtn.frame.size.height/2;
    pickerView1=[[UIPickerView alloc]initWithFrame:CGRectMake(0, 40, self.view.frame.size.width, 200)];
    pickerView1.delegate=self;
    pickerView1.dataSource=self;
    
    [pickerView1 selectRow:0 inComponent:0 animated:YES];
    items=@[@"Load",@"Full"];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"clearFeilds"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(getStatus:)
                                                 name:@"getStatus"
                                               object:nil];
    
}
-(void)viewWillAppear:(BOOL)animated{
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillLayoutSubviews{
    [UserModal addGradient:_scanBtn];
    
}
- (void)receiveTestNotification:(NSNotification *) notification
{
    _pickUpTF.text=nil;
    _dropOffTF.text=nil;
    _loadSizeTF.text=nil;
    _dateTimeTF.text=nil;
}
- (void)getStatus:(NSNotification *) notification
{
    if ([[UserModal sharedInstance]truckerStatusWithResposne]) {
        OnRouteShipmentVC *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"OnRouteShipmentVC"];
        [self.navigationController pushViewController:vc animated:YES];
    }
}
-(void)setUpDatePicker{
    dateFormat = [[NSDateFormatter alloc] init];
    
    
    datePicker =[[UIDatePicker alloc] initWithFrame:CGRectMake(0, 40, self.view.frame.size.width, 200)];
    
    datePicker.datePickerMode=UIDatePickerModeDate;
 //   datePicker.minimumDate=[NSDate date];
    
    [datePicker addTarget:self action:@selector(updateData) forControlEvents:UIControlEventValueChanged];
}
-(void)updateData
{
    
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    
    _dateTimeTF.text= [dateFormat stringFromDate:datePicker.date];
    
}
#pragma mark - pickerView DataSouce&Delegates
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return items.count;
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    [self pickerView:pickerView didSelectRow:0 inComponent:0];
    
    return [items objectAtIndex:row];
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    self.loadSizeTF.text=[items objectAtIndex:row];
    
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 40;
}

#pragma mark TextFeild delegates
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField==_dateTimeTF) {
        _dateTimeTF.inputView=datePicker;
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        
        _dateTimeTF.text= [dateFormat stringFromDate:[NSDate date]];
    }
    
    if (textField==_loadSizeTF) {
        _loadSizeTF.inputView=pickerView1;
    }
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)scanAction:(id)sender {
    if ([self validateFeilds]) {
        
        [self scanShipments];
    }
   // [self performSegueWithIdentifier:@"shipments" sender:nil];
}
- (IBAction)pickUpAction:(id)sender {
    pickup=YES;
    [self openGPlaces];

}
#pragma mark -validation
-(BOOL)validateFeilds{
    NSString *loadSize = [_loadSizeTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString *dateTime = [_dateTimeTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (_pickUpTF.text.length==0) {
        [UserModal showAlert:@"Alert" withMessage:@"Please Enter Pickup Location" withVC:self withPop:NO];
        return NO;
    }
    if (dropoffLongitute.length==0) {
        [UserModal showAlert:@"Alert" withMessage:@"Please Enter Dropoff Location" withVC:self withPop:NO];
        return NO;
    }
    if (dateTime.length==0) {
//        [UserModal showAlert:@"Alert" withMessage:@"Please Enter Date and time" withVC:self withPop:NO];
//        return NO;
    }
    if (loadSize.length==0) {
//        [UserModal showAlert:@"Alert" withMessage:@"Please Enter Load size" withVC:self withPop:NO];
//        return NO;
    }
    
    
    return YES;
}

#pragma mark- Google PlaceApi
-(void)openGPlaces{
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    acController.delegate = self;
    [self presentViewController:acController animated:YES completion:nil];
}
- (void)viewController:(GMSAutocompleteViewController *)viewController
didAutocompleteWithPlace:(GMSPlace *)place {
    [self dismissViewControllerAnimated:YES completion:nil];
    // Do something with the selected place.
    [self getZipCodeAndCountryCode:place.coordinate source:pickup];

    if (pickup) {
        _pickUpTF.text=place.formattedAddress;
        pickUpLat=[NSString stringWithFormat:@"%f",place.coordinate.latitude];
        pickupLongitute=[NSString stringWithFormat:@"%f",place.coordinate.longitude];

    }else{
        _dropOffTF.text=place.formattedAddress;
        dropoffLat=[NSString stringWithFormat:@"%f",place.coordinate.latitude];
        dropoffLongitute=[NSString stringWithFormat:@"%f",place.coordinate.longitude];

    }
}

- (void)viewController:(GMSAutocompleteViewController *)viewController
didFailAutocompleteWithError:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:nil];
    // TODO: handle the error.
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)didUpdateAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}
- (IBAction)dropOffAction:(id)sender {
    pickup=NO;
    [self openGPlaces];
}
-(void)getZipCodeAndCountryCode:(CLLocationCoordinate2D)cordinates source:(BOOL)source{
    newLocation= [[CLLocation alloc] initWithLatitude:cordinates.latitude longitude:cordinates.longitude];
    [geocoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray *placemarks, NSError *error)
     {
         
         if (placemarks && placemarks.count > 0)
         {
             CLPlacemark *placemark = placemarks[0];
             
             NSDictionary *addressDictionary =placemark.addressDictionary;
             if (source) {
                 _pickUpZipTF.text=[addressDictionary valueForKey:@"ZIP"];
             }else{
                 _dropOffZipTF.text=[addressDictionary valueForKey:@"ZIP"];
             }
             // NSArray*formattedAddressLines = [addressDictionary objectForKey:@"FormattedAddressLines"];
             //   NSString*    address = [formattedAddressLines objectAtIndex:0];
         }
     }];
    
}

#pragma mark- web services
-(void)scanShipments{
    [self.view endEditing:YES];
    [SVProgressHUD show];
    [shipmentsArr removeAllObjects];
    NSDictionary *dic=[[NSDictionary alloc]initWithObjectsAndKeys:_pickUpTF.text,@"pickup_city",_dropOffTF.text,@"dropoff_city",pickUpLat,@"pickup_latitude",pickupLongitute,@"pickup_longitude",dropoffLat,@"dropoff_latitude",dropoffLongitute,@"dropoff_longitute",_dateTimeTF.text,@"pickup_date_time",_loadSizeTF.text,@"loadsize",@"Scan Shipment",@"scan_shipment",_pickUpZipTF.text,@"pickup_zipcode",_dropOffZipTF.text,@"dropoff_zipcode", nil];
    
    [[WebService sharedInstance]parameters:dic onComplete:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([[responseObject valueForKey:@"status"] integerValue]==1) {
            
            
            for (int i=0; i<[[responseObject valueForKey:@"result"] count]; i++) {
                if ([[[responseObject valueForKeyPath:@"result.shipment_status"] objectAtIndex:i] integerValue]==0) {
                    [shipmentsArr addObject:[[responseObject valueForKey:@"result"] objectAtIndex:i]];
                }
            }
            if (shipmentsArr.count==0) {
                [UserModal showAlert:@"Alert" withMessage:@"No result found" withVC:self withPop:NO];
                
            }else{
                SelectShipmentVC *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"SelectShipmentVC"];
                vc.responseArr=shipmentsArr;
                [self.navigationController pushViewController:vc animated:YES];
            }
        }else{
            [UserModal showAlert:@"Alert" withMessage:@"No result found" withVC:self withPop:NO];
            
        }
    } onError:^(NSError *error) {
        [SVProgressHUD dismiss];
        [UserModal showAlert:@"Error!!" withMessage:error.localizedDescription withVC:self withPop:NO];
        
    }];
}

@end
