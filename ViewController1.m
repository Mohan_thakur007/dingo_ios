//
//  ViewController1.m
//  Dingo
//
//  Created by MAC on 10/10/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import "ViewController1.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "PointAnnotation.h"
#import "Annotation.h"

@interface ViewController1 ()<MKMapViewDelegate,CLLocationManagerDelegate>


@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLLocation* currentLocation;
@property (strong, nonatomic) CLLocation *location;
@property (strong, nonatomic) NSMutableArray *geofences;
@end

@implementation ViewController1

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    [_locationManager startUpdatingLocation];
    __mapView.delegate=self;

    
    
    [self setUpGeofences];
    
    
}








-(void)setUpGeofences
{
    
    CLLocationCoordinate2D center = CLLocationCoordinate2DMake(30.741482,
                                                               76.768066);
    CLCircularRegion *region = [[CLCircularRegion alloc] initWithCenter:center
                                                                 radius:300.0
                                                             identifier:@"Home"];
     [self.locationManager startMonitoringForRegion:region];
    PointAnnotation *mapPin = [[PointAnnotation alloc] init];
    mapPin.title = @"oluyi";
    mapPin.coordinate = center;
    
    [self._mapView addAnnotation:mapPin];
    MKCircle *circle = [MKCircle circleWithCenterCoordinate:center radius:1000];
    [__mapView addOverlay:circle];
    
    
    if ([region containsCoordinate:center]) {
        NSLog(@"You're soaking in it.");
    }
    
    
     self.locationManager.desiredAccuracy = 300;
    self.locationManager.allowsBackgroundLocationUpdates = YES;
    [self._mapView showsUserLocation];

}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id <MKOverlay>)overlay
{
    MKCircleRenderer *circleView = [[MKCircleRenderer alloc] initWithOverlay:overlay];
    circleView.strokeColor = [UIColor redColor];
    circleView.fillColor = [[UIColor redColor] colorWithAlphaComponent:0.4];
    circleView.lineWidth=5.0;
    return circleView;
}
- (void)showSorryAlert {
    UIAlertController *alert =   [UIAlertController
                                  alertControllerWithTitle:@"Info"
                                  message:@"You are using UIAlertController"
                                  preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)locationManager:(CLLocationManager *)manager
didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
            [self.locationManager requestAlwaysAuthorization];
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            [self.locationManager startUpdatingLocation];
            [self setUpGeofences];
            break;
        case kCLAuthorizationStatusAuthorizedAlways:
            [self.locationManager startUpdatingLocation];
            [self setUpGeofences];
            break;
        case kCLAuthorizationStatusRestricted:
            // restricted by e.g. parental controls. User can't enable Location Services
            break;
        case kCLAuthorizationStatusDenied:
            // user denied your app access to Location Services, but can grant access from Settings.app
            break;
        default:
            break;
    }
}
-(void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error
{
    NSLog(@"Fail");
}

- (void)locationManager:(CLLocationManager *)manager
         didEnterRegion:(CLRegion *)region {
    NSLog(@"didEnter : %@", region);
    //[self displayNotif:@"Bienvenue !" withBody:@"Passez nous voir !"];
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    NSLog(@"didExit : %@", region);
    //[self displayNotif:@"Au revoir !" withBody:@"A bientôt !"];
}

- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region {
    NSLog(@"Start monitoring for region: %@", region.identifier);
    [self.locationManager requestStateForRegion:region];
    [self.locationManager performSelector:@selector(requestStateForRegion:) withObject:region afterDelay:5];
}


- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations {
    
  //  - (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    NSLog(@"NEW LOCATION");
}

//- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
//    
//    // zoom to region containing the user location
//    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800);
//    [self._mapView setRegion:[self._mapView regionThatFits:region] animated:YES];
//    
//    // add the annotation
//    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
//    point.coordinate = userLocation.coordinate;
//    point.title = @"The Location";
//    point.subtitle = @"Sub-title";
//    [self._mapView addAnnotation:point];
//}

    // Stop location updates when they aren't needed anymore
    //[self.locationManager stopUpdatingLocation];
    
    // Disable background location updates when they aren't needed anymore
//    self.locationManager.allowsBackgroundLocationUpdates = NO;



- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region
{
    NSLog(@"Entered");
    NSLog(@"didDetermineState");
//    
//    if (state == CLRegionStateInside) {
//        
//        NSLog(@"inside");
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"inside" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//        [alert show];
//        return;
//        
//        
//    } else if (state == CLRegionStateOutside) {
//        NSLog(@"outside");
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"outside" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//        [alert show];
//        
//    } else {
//        NSLog(@"unknown");
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"unknown" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//        [alert show];
//    }
    
}


    

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
