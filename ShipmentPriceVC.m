//
//  ShipmentPriceVC.m
//  Dingo
//
//  Created by MAC on 06/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import "ShipmentPriceVC.h"
#import "UserModal.h"
#import "ShipmentsVC.h"
#import "PriceCell.h"
#import "SVProgressHUD.h"
#import "WebService.h"
@interface ShipmentPriceVC ()<UITextFieldDelegate>
{
    NSMutableArray *selectedArr;
}
@end

@implementation ShipmentPriceVC

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.automaticallyAdjustsScrollViewInsets = false;
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Model Motor Carrier-Broker Agreement" ofType:@"doc"];
    NSURL *targetURL = [NSURL fileURLWithPath:path];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    self.webView.autoresizesSubviews = YES;
    
    _webView.scalesPageToFit = YES;
    _webView.backgroundColor=[UIColor clearColor];
    [_webView loadRequest:request];
    // Do any additional setup after loading the view.
    selectedArr=[NSMutableArray new];
    _tableView.tableFooterView=[UIView new];
    _submitBtn.layer.cornerRadius=_submitBtn.frame.size.height/2;
    _shipmentPrice.text=[NSString stringWithFormat:@"Estimed price for delivery :- $ %@",_estimatedCost];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillLayoutSubviews{
    [UserModal addGradient:_submitBtn];

}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"allShipments"]) {
        ShipmentsVC *vc=[segue destinationViewController];
        vc.fromCreateShipments=YES;
    }
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField==_ownPriceTF) {
        _ownPriceTF.text=@"$";
    }
}
#pragma mark- UITableView Datasource


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    PriceCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if ([selectedArr containsObject:[NSNumber numberWithInt:(int)indexPath.row]]) {
        cell.selectionImg.image=[UIImage imageNamed:@"green_selected"];
    }else{
        cell.selectionImg.image=[UIImage imageNamed:@"green_unselected"];
        
    }
    cell.selectionBtn.tag=indexPath.row;
    return cell;
}
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (IBAction)submitAction:(id)sender {
    if ([_acceptbtn isSelected]) {
        [SVProgressHUD show];
        [[WebService sharedInstance]parameters:_postDic onComplete:^(id responseObject) {
            [SVProgressHUD dismiss];
            if ([[responseObject valueForKey:@"status"] integerValue]==1) {
                [self performSegueWithIdentifier:@"allShipments" sender:nil];
                
            }else{
                
            }
        } onError:^(NSError *error) {
            [SVProgressHUD dismiss];
            [UserModal showAlert:@"Error!!" withMessage:error.localizedDescription withVC:self withPop:NO];
            
        }];
        
    }else{
        [UserModal showAlert:@"Alert" withMessage:@"Please accept terms and conditions" withVC:self withPop:NO];
    }
}
- (IBAction)acceptTermsCondtionsAction:(id)sender {
    if (![sender isSelected]) {
        [sender setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
        [sender setSelected:YES];
        
    }else{
        [sender setSelected:NO];
        [sender setImage:[UIImage imageNamed:@"uncheck"] forState:UIControlStateNormal];
        
    }
}

- (IBAction)selectedPriceAction:(id)sender {
    [selectedArr removeAllObjects];
    [selectedArr addObject:[NSNumber numberWithInt:(int)[sender tag]]];
    [_tableView reloadData];
}
@end
