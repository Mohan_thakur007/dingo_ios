//
//  ShipmentDetailVC.h
//  Dingo
//
//  Created by mac on 7/14/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface ShipmentDetailVC : UIViewController
@property NSArray *shipmentDeatil;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UILabel *sourceLbl;
@property (weak, nonatomic) IBOutlet UILabel *destinationLbl;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;
@property (weak, nonatomic) IBOutlet UILabel *loadSizeLbl;
@property (weak, nonatomic) IBOutlet UITextField *shipmentNumberLbl;
@property (weak, nonatomic) IBOutlet UILabel *shipmentPriceLbl;
@property (weak, nonatomic) IBOutlet UIButton *trackBtn;
- (IBAction)trackAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *shipmentNameLbl;
@property (weak, nonatomic) IBOutlet UIView *vview;
@property BOOL trucker;
@property NSString *shipmentStatus;

@end
