//
//  TrcukerScanShipmentsVC.h
//  Dingo
//
//  Created by MAC on 06/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrcukerScanShipmentsVC : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *scanBtn;
- (IBAction)scanAction:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *dateTimeTF;
@property (strong, nonatomic) IBOutlet UITextField *loadSizeTF;
- (IBAction)pickUpAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *pickUpZipTF;
@property (weak, nonatomic) IBOutlet UITextField *dropOffZipTF;

- (IBAction)dropOffAction:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *pickUpTF;
@property (strong, nonatomic) IBOutlet UITextField *dropOffTF;
@end
