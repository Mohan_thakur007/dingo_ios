//
//  AdminVC.h
//  Dingo
//
//  Created by MAC on 06/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdminVC : UIViewController
@property (strong, nonatomic) IBOutlet UIView *shadowView;
@property (strong, nonatomic) IBOutlet UIButton *printDetailBtn;
@property (strong, nonatomic) IBOutlet UIButton *userDetailBtn;

@property (strong, nonatomic) IBOutlet UIButton *trackBtn;
@property (strong, nonatomic) IBOutlet UIButton *documentBtn;
@property (strong, nonatomic) IBOutlet UIButton *trackShipmentBtn;
@end
