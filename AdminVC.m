//
//  AdminVC.m
//  Dingo
//
//  Created by MAC on 06/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import "AdminVC.h"
#import "UserModal.h"
@interface AdminVC ()

@end

@implementation AdminVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _trackBtn.layer.cornerRadius=_trackBtn.layer.frame.size.height/2;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillLayoutSubviews{
    [UserModal addGradient:_trackBtn];
    [UserModal addGradient:_documentBtn];
    [UserModal addGradient:_userDetailBtn];
    
    [UserModal addShadow:_userDetailBtn];
    [UserModal addShadow:_trackShipmentBtn];
    [UserModal addShadow:_printDetailBtn];
    [UserModal addShadow:_documentBtn];
    [UserModal addShadowToView:_shadowView];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
