//
//  UploadBillsVC.h
//  Dingo
//
//  Created by MAC on 06/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UploadBillsVC : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *uploadBillsBtn;
@property (strong, nonatomic) IBOutlet UIButton *signedRecieptBtn;
- (IBAction)uploadBillsAction:(id)sender;
- (IBAction)uploadReceiptAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *notifyBtn;
- (IBAction)notifyAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *cancelBtn;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *menuBtn;

- (IBAction)cancelAction:(id)sender;
@property BOOL from;
@end
