//
//  RatingVC.h
//  Dingo
//
//  Created by MAC on 10/9/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RatingVC : UIViewController


@property (strong, nonatomic) IBOutlet UITextView *feedbackTextView;
@property (strong, nonatomic) IBOutlet UIButton *submitAction;



@end
