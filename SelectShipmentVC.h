//
//  SelectShipmentVC.h
//  Dingo
//
//  Created by MAC on 06/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface SelectShipmentVC : UIViewController



@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentControl;

@property (strong, nonatomic) IBOutlet MKMapView *mapView;


@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *notifyShipperBtn;
- (IBAction)notifyShipperAction:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *searchAddressTF;
@property (strong, nonatomic) IBOutlet UIView *searchView;
- (IBAction)selectShipmentAction:(id)sender;
- (IBAction)segmentControlAction:(id)sender;
@property NSArray *responseArr;
@end
