//
//  SelectShipmentVC.m
//  Dingo
//
//  Created by MAC on 06/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import "SelectShipmentVC.h"
#import "TruckerSelectShipmentCell.h"
#import "UserModal.h"
#import "UploadBillsVC.h"
#import "SVProgressHUD.h"
#import "WebService.h"
#import "OnRouteShipmentVC.h"
#import "ShipmentDetailVC.h"
#import "PointAnnotation.h"
#import "Annotation.h"
@interface SelectShipmentVC ()<MKMapViewDelegate,CLLocationManagerDelegate>
{
    NSArray *selectedArr;
    NSString *shipmentId;
    NSUInteger indexx;
    NSMutableArray *arrCoordinateStr;
}
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLLocation* currentLocation;


@end

@implementation SelectShipmentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _notifyShipperBtn.enabled=NO;
    _notifyShipperBtn.alpha=0.5;
    // Do any additional setup after loading the view.
    _tableView.tableFooterView=[UIView new];
    _notifyShipperBtn.layer.cornerRadius=_notifyShipperBtn.frame.size.height/2;
    [UserModal addShadowToView:_searchView];
    _tableView.hidden=NO;
    _mapView.hidden=YES;
    
    
    self.mapView.delegate =  self;
    self.mapView.showsUserLocation = YES;
    _mapView.userTrackingMode=YES;
    if ([CLLocationManager locationServicesEnabled] )
    {
        if (self.locationManager == nil )
        {
            self.locationManager = [[CLLocationManager alloc] init];
            self.locationManager.delegate = self;
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            self.locationManager.distanceFilter = kCLDistanceFilterNone;
        }
        
        [self.locationManager startUpdatingLocation];
        [self mapLocationSet:self.locationManager.location];
    }

    [self addAllPins];

 
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)mapLocationSet:(CLLocation*)locationn
{
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.01;
    span.longitudeDelta = 0.01;
    region.span = span;
    region.center =locationn.coordinate;
    [self.mapView setRegion:region animated:YES];
     [_mapView setCenterCoordinate:locationn.coordinate animated:YES];
}

-(void)viewWillLayoutSubviews{
    [UserModal addGradient:_notifyShipperBtn];
}

-(void)addAllPins
{
    self.mapView.delegate=self;
    
     NSMutableArray *name = [NSMutableArray array];
      for(int i = 0; i < _responseArr.count ; i++)
      {
   
    [name addObject:[[_responseArr valueForKey:@"pickup_address" ]objectAtIndex:i]];
            NSLog(@"%@", name);
          NSLog(@"%lu", (unsigned long)[name count]);
    
      }
   arrCoordinateStr = [NSMutableArray array];
     for(int i = 0; i < name.count ; i++)
     {
            [arrCoordinateStr addObject:[NSString stringWithFormat:@"%@,%@",[[_responseArr valueForKey:@"pickup_latitude"]objectAtIndex:i],[[_responseArr valueForKey:@"pickup_longitude"]objectAtIndex:i]]];
           NSLog(@"%lu", (unsigned long)[arrCoordinateStr count]);
//
     }
    NSLog(@"%@", arrCoordinateStr);
    
      
    for(int i = 0; i < name.count; i++)
    {
        [self addPinWithTitle:name[i] AndCoordinate:arrCoordinateStr[i] tag:i];
    }
      
}


-(void)addPinWithTitle:(NSString *)title AndCoordinate:(NSString *)strCoordinate tag:(NSInteger)tag;
{
    PointAnnotation *mapPin = [[PointAnnotation alloc] init];
    mapPin.tags=tag;
    
    // clear out any white space
    strCoordinate = [strCoordinate stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    // convert string into actual latitude and longitude values
    NSArray *components = [strCoordinate componentsSeparatedByString:@","];
    
    double latitude = [components[0] doubleValue];
    double longitude = [components[1] doubleValue];
    
    // setup the map pin with all data and add to map view
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longitude);
   
    mapPin.title = title;
    
    mapPin.coordinate = coordinate;
    
    [self.mapView addAnnotation:mapPin];
    [self zoomToFitMapAnnotations:_mapView insideArray:_mapView.annotations];
}

-(void)zoomToFitMapAnnotations:(MKMapView*)mapView insideArray:(NSArray*)anAnnotationArray
{
    if([_mapView.annotations count] == 0)
        return;
    
    CLLocationCoordinate2D topLeftCoord;
    topLeftCoord.latitude = -30;
    topLeftCoord.longitude = 100;
    
    CLLocationCoordinate2D bottomRightCoord;
    bottomRightCoord.latitude = 30;
    bottomRightCoord.longitude = -100;
    
    for(PointAnnotation* annotation in anAnnotationArray)
    {
        topLeftCoord.longitude = fmin(topLeftCoord.longitude, annotation.coordinate.longitude);
        topLeftCoord.latitude = fmax(topLeftCoord.latitude, annotation.coordinate.latitude);
        
        bottomRightCoord.longitude = fmax(bottomRightCoord.longitude, annotation.coordinate.longitude);
        bottomRightCoord.latitude = fmin(bottomRightCoord.latitude, annotation.coordinate.latitude);
    }
    
    MKCoordinateRegion region;
    region.center.latitude = topLeftCoord.latitude - (topLeftCoord.latitude - bottomRightCoord.latitude) * 0.01;
    region.center.longitude = topLeftCoord.longitude + (bottomRightCoord.longitude - topLeftCoord.longitude) * 0.01;
    region.span.latitudeDelta = fabs(topLeftCoord.latitude - bottomRightCoord.latitude) * 0.5; // Add a little extra space on the sides
    region.span.longitudeDelta = fabs(bottomRightCoord.longitude - topLeftCoord.longitude) * 0.5; // Add a little extra space on the sides
    
    region = [_mapView regionThatFits:region];
    [_mapView setRegion:region animated:YES];
}







- (MKAnnotationView *)mapView:(MKMapView *)map viewForAnnotation:(id <MKAnnotation>)annotation
{
    if (annotation == _mapView.userLocation) return nil;
    
    Annotation *pin = (Annotation *) [_mapView dequeueReusableAnnotationViewWithIdentifier: @"CustomAnnotation"];
    PointAnnotation *an=(PointAnnotation*)annotation;
    if (pin == nil)
    {
        pin = [[Annotation alloc] initWithAnnotation: annotation reuseIdentifier: @"CustomAnnotation"];
    }
    else
    {
        pin.annotation = annotation;
    }
    pin.tag=an.tags;
    pin.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    //pin.tintColor = MKPinAnnotationColorRed;
    // pin.animatesDrop = YES;
    [pin setEnabled:YES];
    [pin setCanShowCallout:YES];
    pin.image = [UIImage imageNamed:@"address"];
    return pin;
}
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    NSLog(@"%f",[[view annotation] coordinate].latitude);
    NSLog(@"%f",[[view annotation] coordinate].longitude);
    
    UIAlertController *alertC = [UIAlertController alertControllerWithTitle:@"Shipment Detail" message:@"Do you want to check detail of shipment" preferredStyle:UIAlertControllerStyleAlert];
    
    
    
    
    [alertC addAction:[UIAlertAction actionWithTitle:@"Detail"  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        ShipmentDetailVC *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"ShipmentDetailVC"];
        vc.shipmentDeatil=[_responseArr objectAtIndex:view.tag];
        vc.trucker=YES;
        [self.navigationController pushViewController:vc animated:YES];
    }]];
    
    [alertC addAction:[UIAlertAction actionWithTitle:@"Cancel"  style:UIAlertActionStyleCancel handler:^(UIAlertAction * action){
        
    }]];
    
    [self presentViewController: alertC animated:YES completion:nil];
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"bills"]) {
        UploadBillsVC *vc=[segue destinationViewController];
        vc.from=YES;
    }
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

#pragma mark- UITableView Datasource


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return _responseArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    TruckerSelectShipmentCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    [UserModal addShadowToView:cell.cellView];
    if ([selectedArr containsObject:[NSNumber numberWithInt:(int)indexPath.row]]) {
        [cell.selectionBtn setImage:[UIImage imageNamed:@"blue_selected"] forState:UIControlStateNormal];
    }else{
        [cell.selectionBtn setImage:[UIImage imageNamed:@"blue_unselected"] forState:UIControlStateNormal];

    }
    cell.pickupLbl.text=[[_responseArr valueForKey:@"pickup_address"]objectAtIndex:indexPath.row];
    cell.priceLbl.text=[NSString stringWithFormat:@"$%.0f",[[[_responseArr valueForKey:@"trucker_price"]objectAtIndex:indexPath.row] floatValue]];
    cell.timeLbl.text=[[_responseArr valueForKey:@"pickup_window"] objectAtIndex:indexPath.row];
    cell.selectionBtn.tag=indexPath.row;

    return cell;
}
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    ShipmentDetailVC *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"ShipmentDetailVC"];
    vc.shipmentDeatil=[_responseArr objectAtIndex:indexPath.row];
    vc.trucker=YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)notifyShipperAction:(id)sender {
    if (shipmentId) {
        ShipmentDetailVC *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"ShipmentDetailVC"];
        vc.shipmentDeatil=[_responseArr objectAtIndex:indexx];
        vc.trucker=YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
  }
- (IBAction)selectShipmentAction:(id)sender {
    selectedArr=[[NSArray alloc]initWithObjects:[NSNumber numberWithInt:(int)[sender tag]], nil];
    shipmentId=[[_responseArr valueForKey:@"id"]objectAtIndex:[sender tag]];
    [_tableView reloadData];
    indexx=[sender tag];
    _notifyShipperBtn.enabled=YES;
    _notifyShipperBtn.alpha=1.0;

}

- (IBAction)segmentControlAction:(id)sender {
    switch (self.segmentControl.selectedSegmentIndex)
    {
            case 0:
            _tableView.hidden=NO;
            
            break;
            
            case 1:
            _tableView.hidden=YES;
            _mapView.hidden=NO;
            
            break;
            
            default:
            _tableView.hidden=NO;
            break;
            
            
            
    }
    
    
    
    
    
    
    
    
    
}
#pragma mark- webservice
-(void)pickJob{
    [SVProgressHUD show];
     NSDictionary *dic=[[NSDictionary alloc]initWithObjectsAndKeys:[UserModal getUserId],@"user_id",shipmentId,@"shipment_id",@"Pick Shipment",@"pick_shipment", nil];
    [[WebService sharedInstance]parameters:dic onComplete:^(id responseObject) {
        [SVProgressHUD dismiss];

        if ([[responseObject valueForKey:@"status"]integerValue]==1) {

         //   [UserModal showAlert:@"Message" withMessage:@"Successfully picked the job" withVC:self withPop:YES];
            OnRouteShipmentVC *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"OnRouteShipmentVC"];
            vc.shipmentNumber=[[_responseArr valueForKey:@"shipment_number"]objectAtIndex:indexx];
            vc.shimpmentID=[[_responseArr valueForKey:@"id"]objectAtIndex:indexx];
            vc.shipmentDetail=[_responseArr objectAtIndex:indexx];
            [self.navigationController pushViewController:vc animated:YES];

        }else{
            [UserModal showAlert:@"Alert" withMessage:[responseObject valueForKey:@"message"] withVC:self withPop:NO];
        }
    } onError:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
-(void)popToHome{
    
}
@end
