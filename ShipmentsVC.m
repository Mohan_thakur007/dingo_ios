//
//  ShipmentsVC.m
//  Dingo
//
//  Created by MAC on 05/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import "ShipmentsVC.h"
#import "ShipmentCell.h"
#import "UserModal.h"
#import "SVProgressHUD.h"
#import "WebService.h"
#import "ShipmentDetailVC.h"
#import "TrackShipmentVC.h"
@interface ShipmentsVC ()
{
    NSArray *responseArr;
    NSTimer *timer;
}
@end

@implementation ShipmentsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getShipments:NO];
    // Do any additional setup after loading the view.
    _okBtn.layer.cornerRadius=_okBtn.frame.size.height/2;
//    if (_fromCreateShipments) {
//        self.navigationItem.leftBarButtonItems=nil;
//
//    }
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor clearColor];
    self.refreshControl.tintColor = [UIColor blackColor];
    [self.refreshControl addTarget:self
                            action:@selector(refrfesh)
                  forControlEvents:UIControlEventValueChanged];
    
    [_tableView addSubview:_refreshControl];
}
-(void)refrfesh{
    [self getShipments:YES];
}
//-(void) viewWillDisappear:(BOOL)animated
//{
//    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound)
//    {
//        [self backButtonPressed];
//        [self.navigationController popViewControllerAnimated:NO];
//    }
//    
//    [super viewWillDisappear:animated];
//}
//
//-(void)backButtonPressed
//{
//    NSLog(@"YEA");
//}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillLayoutSubviews{
    [UserModal addGradient:_okBtn];
}
#pragma mark- UITableView Datasource


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return responseArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    ShipmentCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.shipmentName.text=[[responseArr valueForKey:@"shipment_name"] objectAtIndex:indexPath.row];
    [UserModal addShadowToView:cell.cellView];
    [UserModal addGradient:cell.trackBtn];
    if ([[[responseArr valueForKey:@"shipment_status"] objectAtIndex:indexPath.row]intValue]==0) {
        [cell.statusBtn setTitle:@"Created" forState:UIControlStateNormal];
        cell.trackBtn.hidden=YES;
    }else if ([[[responseArr valueForKey:@"shipment_status"] objectAtIndex:indexPath.row]intValue]==1){
        [cell.statusBtn setTitle:@"Confirmed" forState:UIControlStateNormal];
        cell.trackBtn.hidden=YES;

    }else if ([[[responseArr valueForKey:@"shipment_status"] objectAtIndex:indexPath.row]intValue]==2){
        [cell.statusBtn setTitle:@"Arrived to Pickup" forState:UIControlStateNormal];
        cell.trackBtn.hidden=YES;
        
    }else if ([[[responseArr valueForKey:@"shipment_status"] objectAtIndex:indexPath.row]intValue]==3){
        [cell.statusBtn setTitle:@"Picked" forState:UIControlStateNormal];
        cell.trackBtn.hidden=NO;

    }else if ([[[responseArr valueForKey:@"shipment_status"] objectAtIndex:indexPath.row]intValue]==4){
        [cell.statusBtn setTitle:@"In Transit" forState:UIControlStateNormal];
        cell.trackBtn.hidden=NO;
        
    }else if ([[[responseArr valueForKey:@"shipment_status"] objectAtIndex:indexPath.row]intValue]==5){
        [cell.statusBtn setTitle:@"Halt" forState:UIControlStateNormal];
        cell.trackBtn.hidden=NO;
        
    }else if ([[[responseArr valueForKey:@"shipment_status"] objectAtIndex:indexPath.row]intValue]==6){
        [cell.statusBtn setTitle:@"In Transit" forState:UIControlStateNormal];
        cell.trackBtn.hidden=NO;
        
    }else if ([[[responseArr valueForKey:@"shipment_status"] objectAtIndex:indexPath.row]intValue]==7){
        [cell.statusBtn setTitle:@"Arrived" forState:UIControlStateNormal];
        cell.trackBtn.hidden=NO;
        
    }else if ([[[responseArr valueForKey:@"shipment_status"] objectAtIndex:indexPath.row]intValue]==8){
        [cell.statusBtn setTitle:@"Delivered" forState:UIControlStateNormal];
        cell.trackBtn.hidden=NO;
        
    }else if ([[[responseArr valueForKey:@"shipment_status"] objectAtIndex:indexPath.row]intValue]==9){
        [cell.statusBtn setTitle:@"Closed" forState:UIControlStateNormal];
        cell.trackBtn.hidden=YES;
        
    }
    cell.trackBtn.tag=indexPath.row;
    return cell;
}
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    ShipmentCell *cell=[tableView cellForRowAtIndexPath:indexPath];
        ShipmentDetailVC *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"ShipmentDetailVC"];
        vc.shipmentDeatil=[responseArr objectAtIndex:indexPath.row];
    vc.shipmentStatus=cell.statusBtn.titleLabel.text;
        [self.navigationController pushViewController:vc animated:YES];
       // [self performSegueWithIdentifier:@"invoice" sender:nil];

}




- (IBAction)okAction:(id)sender {
}
#pragma mark- webServices

-(void)getShipments:(BOOL)refresh{
    if (!refresh) {
        [SVProgressHUD show];

    }
    NSDictionary *dic=[[NSDictionary alloc]initWithObjectsAndKeys:[UserModal getUserId],@"user_id",@"Get Shipment",@"get_shipment", nil];
    [[WebService sharedInstance]parameters:dic onComplete:^(id responseObject) {
        [SVProgressHUD dismiss];
        [_refreshControl endRefreshing];

        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            responseArr=[responseObject valueForKey:@"result"];
            [_tableView reloadData];
        }else{
            [UserModal showAlert:@"Alert" withMessage:[responseObject valueForKey:@"message"] withVC:self withPop:NO];
        }
    } onError:^(NSError *error) {
        [_refreshControl endRefreshing];

        [SVProgressHUD dismiss];
    }];
}
- (IBAction)statusAction:(id)sender {
}

- (IBAction)trackAction:(id)sender {
    TrackShipmentVC *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"TrackShipmentVC"];
    vc.shipmentNumber=[[responseArr valueForKey:@"shipment_number"] objectAtIndex:[sender tag]];
    vc.fromHome=YES;
    [self.navigationController pushViewController:vc animated:YES];

}

- (IBAction)detailAction:(id)sender {
}
@end
