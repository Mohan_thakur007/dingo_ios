//
//  WebService.h
//  SingletonExample

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@interface WebService : NSObject

@property (nonatomic, strong) id (^responseObject)(void);

+ (instancetype)sharedInstance;

-(void)parameters:(NSDictionary *)paramters  onComplete:(void (^)(id responseObject))successBlock
          onError:(void (^)(NSError *error))errorBlock;

-(void) parameters:(NSDictionary *)paramters image:(NSData*)image imageparameter:(NSString*)imageparamter onComplete:(void (^)(id responseObject))successBlock
           onError:(void (^)(NSError *error))errorBlock;

@end
