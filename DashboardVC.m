//
//  DashboardVC.m
//  Dingo
//
//  Created by MAC on 10/12/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import "DashboardVC.h"
#import "DashboardCell.h"
#import "UserModal.h"
#import "OnRouteShipmentVC.h"
@interface DashboardVC ()<UICollectionViewDelegate,UICollectionViewDataSource>
{
    NSArray *shipperOptions;
    NSArray *truckerOptions;
    NSArray *bannerImages;
    NSTimer *timer;
    NSInteger index;
    NSArray *iconsArr;
}
@end

@implementation DashboardVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    shipperOptions=@[@"Create Shipment",@"Shipments",@"Settings",@"Profile"];
    truckerOptions=@[@"Scan Shipment",@"Upload Bills",@"Settings",@"Profile"];
    bannerImages=@[@"trucker_image02",@"truck05",@"trucker06"];
    iconsArr=@[@"truck-2",@"truck_img",@"settings-1",@"profile"];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(getStatus:)
                                                 name:@"getStatus"
                                               object:nil];
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden=NO;
    timer= [NSTimer scheduledTimerWithTimeInterval:2.0
                                            target:self
                                          selector:@selector(targetMethod)
                                          userInfo:nil
                                           repeats:YES];
}
-(void)targetMethod{
    index++;
    
        if (index>bannerImages.count-1) {
            index=0;
        }
  
    
    [(UIPageControl*)[self.view viewWithTag:23]setCurrentPage:index];
    [_upperCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0] atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
    [_upperCollectionView reloadData];
}
-(void)viewWillDisappear:(BOOL)animated{
    [timer invalidate];
}
- (void)getStatus:(NSNotification *) notification
{
    if ([[UserModal sharedInstance]truckerStatusWithResposne]) {
        OnRouteShipmentVC *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"OnRouteShipmentVC"];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    for (UICollectionViewCell *cell in [self.upperCollectionView visibleCells]) {
        NSIndexPath *indexPath = [self.upperCollectionView indexPathForCell:cell];
        // NSLog(@"%@",indexPath);
        index=indexPath.row;
        [(UIPageControl*)[self.view viewWithTag:23]setCurrentPage:index];
    }
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section;
{
    if (collectionView==_upperCollectionView) {
        return bannerImages.count;
    }else{
    return 4;
    }
}


- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    
    DashboardCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    if (collectionView==_upperCollectionView) {
        
        [(UIImageView *)[cell viewWithTag:111]setImage:[UIImage imageNamed:[bannerImages objectAtIndex:indexPath.row]]];

        
    }else{
        cell.layer.borderColor=[UIColor lightGrayColor].CGColor;
        cell.layer.borderWidth=0.5;
        if ([[UserModal getUserType] integerValue]==2) {
            cell.menuLabel.text=[truckerOptions objectAtIndex:indexPath.item];
            
        }
        else
        {
            cell.menuLabel.text=[shipperOptions objectAtIndex:indexPath.item];
            
            
        }
        cell.iconImgs.image=[UIImage imageNamed:[iconsArr objectAtIndex:indexPath.row]];
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
    
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    UIViewController *vc;
    if (collectionView!=_upperCollectionView) {
        
        if ([[UserModal getUserType] integerValue]==2){
            
            switch (indexPath.row) {
                    
                    
                case 0:
                {
                    
                    vc=[self.storyboard instantiateViewControllerWithIdentifier:@"TrcukerScanShipmentsVC"];
                    [self.navigationController pushViewController:vc animated:YES];
                    
                    
                }
                    break;
                case 1:
                {
                    vc=[self.storyboard instantiateViewControllerWithIdentifier:@"UploadBillsVC"];
                    [self.navigationController pushViewController:vc animated:YES];
                }
                    break;
                case 2:
                {
                    vc=[self.storyboard instantiateViewControllerWithIdentifier:@"SettingVC"];
                    [self.navigationController pushViewController:vc animated:YES];
                    
                }
                    break;
                case 3:
                {
                    vc=[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileVC"];
                    [self.navigationController pushViewController:vc animated:YES];
                    
                }
                    break;
                    
                    
                default:
                    break;
                    
            }
        }else{
            switch (indexPath.item) {
                    
                case 0:
                {
                    
                    vc=[self.storyboard instantiateViewControllerWithIdentifier:@"CreateShipmentVC"];
                    [self.navigationController pushViewController:vc animated:YES];
                    
                    
                }
                    break;
                case 1:
                {
                    vc=[self.storyboard instantiateViewControllerWithIdentifier:@"ShipmentsVC"];
                    [self.navigationController pushViewController:vc animated:YES];
                }
                    break;
                case 2:
                {
                    vc=[self.storyboard instantiateViewControllerWithIdentifier:@"SettingVC"];
                    [self.navigationController pushViewController:vc animated:YES];
                    
                }
                    break;
                case 3:
                {
                    vc=[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileVC"];
                    [self.navigationController pushViewController:vc animated:YES];
                    
                }
                    break;
                    
                    
                default:
                    break;
                    
            }
            
        }
    }
    
}



- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView==_upperCollectionView) {
       return CGSizeMake(collectionView.frame.size.width,collectionView.frame.size.height);
    }else{
    CGFloat screenWidth = collectionView.frame.size.width;
    CGFloat screenHeight=collectionView.frame.size.height;
    float cellWidth = screenWidth/2.0;
    float cellHeight=screenHeight/2.01;
    
    
    return CGSizeMake(cellWidth, cellHeight);
    }
}
- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
