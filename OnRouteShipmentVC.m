//
//  OnRouteShipmentVC.m
//  Dingo
//
//  Created by mac on 7/13/17.
//  Copyright © 2017 MAC. All rights reserved.
//
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,0)
#define kBaseUrl @"https://maps.googleapis.com/maps/api/directions/json?"

#import "OnRouteShipmentVC.h"
#import "UserModal.h"
#import "WebService.h"
#import "SVProgressHUD.h"

@interface OnRouteShipmentVC ()<CLLocationManagerDelegate,MKMapViewDelegate>
{
    MKPointAnnotation *sourceAnnotation;
    MKPointAnnotation *destAnnotation;
    NSTimer *currentTimer;
    
    
}
@property(strong,nonatomic)CLLocationManager *locationManager;
@property (nonatomic, strong) NSMutableArray *geotifications;
@property(strong,nonatomic) CLCircularRegion *region;
@end

@implementation OnRouteShipmentVC

- (void)viewDidLoad {
    _blurView.hidden=YES;
    _haltView.transform=CGAffineTransformMakeScale(0.0, 0.0);
    _closeBtn.transform=CGAffineTransformMakeScale(0.0, 0.0);

    _haultBtn.layer.cornerRadius=_haultBtn.frame.size.height/2;
    _closeBtn.layer.cornerRadius=_closeBtn.frame.size.height/2;
    _closeBtn.clipsToBounds=YES;
    _haltView.layer.borderColor=[UserModal staticAppColor].CGColor;
    _haltView.layer.borderWidth=1.0;
    _haltTxtView.layer.borderColor=[UIColor darkGrayColor].CGColor;
    _haltTxtView.layer.borderWidth=1.0;
    [self setbuttonTitle:_shipmentStatus];
  //  [self route_show_method];
    _detailView.layer.cornerRadius=15.0;
    [super viewDidLoad];
    _mapView.delegate=self;
    _mapView.showsUserLocation=YES;
    _mapView.userTrackingMode=YES;
    // Do any additional setup after loading the view.`
    
    if ([CLLocationManager locationServicesEnabled] )
    {
        if (self.locationManager == nil )
        {
            self.locationManager = [[CLLocationManager alloc] init];
            self.locationManager.delegate = self;
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            self.locationManager.distanceFilter = kCLDistanceFilterNone;
            
        }
               [self.locationManager startUpdatingLocation];
    }
  //  currentTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(theActionMethod) userInfo:nil repeats:YES];
    [self setUpGeoFence];

  
}

-(void)setUpGeoFence{
    [self.locationManager requestAlwaysAuthorization];
    [self.locationManager requestWhenInUseAuthorization];
    CLLocationCoordinate2D center = CLLocationCoordinate2DMake(30.704649,
                                                               76.717873);
    self.region= [[CLCircularRegion alloc] initWithCenter:center
                                                                 radius:600.0
                                                             identifier:@"Home"];
    MKPointAnnotation *pin = [[MKPointAnnotation alloc] init];
    pin.coordinate = center;
    pin.title =@"Shipper";
    pin.subtitle = [NSString stringWithFormat:@"Radius: %.2f meters",600.00];
    [self.mapView addAnnotation:pin];
    
    // Add the circle indicating radius
    MKCircle *circle = [MKCircle circleWithCenterCoordinate:center radius:600];
    [self.mapView addOverlay:circle];
    [self.locationManager startMonitoringForRegion:self.region];
     //[self.locationManager requestStateForRegion:self.region];
    //    self.locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers;
//    self.locationManager.allowsBackgroundLocationUpdates = YES;
}

-(void)theActionMethod{
    self.locationManager=nil;
   self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    [self updateLocation:[self.locationManager location]];

}
-(void)viewWillLayoutSubviews{
    [UserModal addGradient:_completeBtn];
    [UserModal addGradient:_submitBtn];

}
-(void)viewWillDisappear:(BOOL)animated{
    [currentTimer invalidate];
    currentTimer=nil;

}
-(void)viewWillAppear:(BOOL)animated{
    if ([[UserModal sharedInstance]truckerStatusWithResposne]) {
        _shipmentStatus=[[[[UserModal sharedInstance]truckerStatusWithResposne] valueForKey:@"tracker_shipment_status"] integerValue]+1;
        _shipmentDetail=[[UserModal sharedInstance]truckerStatusWithResposne];
        _shimpmentID=[[[UserModal sharedInstance]truckerStatusWithResposne] valueForKey:@"id"];
        _shipmentNumber=[[[UserModal sharedInstance]truckerStatusWithResposne] valueForKey:@"shipment_number"];
       // if ([[[[UserModal sharedInstance]truckerStatusWithResposne] valueForKey:@"tracker_shipment_status"] integerValue]==1) {
            //[_completeBtn setTitle:@"Pickup Shipment" forState:UIControlStateNormal];
            [self setbuttonTitle:_shipmentStatus];
//        }else{
//            [self setbuttonTitle:[[[[UserModal sharedInstance]truckerStatusWithResposne] valueForKey:@"tracker_shipment_status"] integerValue]];
//        }
        
    }
    _fromLabel.text=[NSString stringWithFormat:@"From: %@",[_shipmentDetail valueForKey:@"pickup_address"] ];
    _toLabel.text=[NSString stringWithFormat:@"To: %@",[_shipmentDetail valueForKey:@"dropoff_address"] ];
    _priceLbl.text=[NSString stringWithFormat:@"$%.0f",[[_shipmentDetail valueForKey:@"trucker_price"] floatValue]];


    self.title=@"On Route";
    self.navigationItem.hidesBackButton=NO;
    _completeBtn.layer.cornerRadius=_completeBtn.frame.size.height/2;
}
-(void)setbuttonTitle:(NSInteger)status{
    _reasonLbl.hidden=YES;
     if (status==2){
        [_completeBtn setTitle:@"Reached to Pick up" forState:UIControlStateNormal];
         _haultBtn.hidden=YES;

     }else if (status==3){
         [_completeBtn setTitle:@"Picked" forState:UIControlStateNormal];
         _haultBtn.hidden=YES;

     }else if (status==4){
         [_completeBtn setTitle:@"In Transit" forState:UIControlStateNormal];
         _haultBtn.hidden=NO;
         
     }else if (status==5){
//         _reasonLbl.hidden=NO;
//         [_haultBtn setTitle:@"Halted" forState:UIControlStateNormal];
//         _haultBtn.enabled=NO;
//         _haultBtn.hidden=NO;
//         _haultBtn.alpha=0.5;
         [_completeBtn setTitle:@"Reached To Destination" forState:UIControlStateNormal];
         _shipmentStatus=8;

     }else if (status==6){
         _reasonLbl.hidden=NO;
         [_haultBtn setTitle:@"Halted" forState:UIControlStateNormal];
         _haultBtn.enabled=NO;
         _haultBtn.hidden=NO;
         _haultBtn.alpha=0.5;
         [_completeBtn setTitle:@"Resume Shipment" forState:UIControlStateNormal];
         
     }else if (status==7){
         _haultBtn.hidden=NO;
         [_haultBtn setTitle:@"Halt" forState:UIControlStateNormal];
         _haultBtn.enabled=YES;
         _haultBtn.alpha=1.0;
         [_completeBtn setTitle:@"Reached To Destination" forState:UIControlStateNormal];
     }else if(status==8){
         _haultBtn.hidden=NO;

         [_completeBtn setTitle:@"Delivered" forState:UIControlStateNormal];
         _shipmentStatus=9;
     }else if (status==9){
         _haultBtn.hidden=NO;
         
         [_completeBtn setTitle:@"Delivered" forState:UIControlStateNormal];
     }
}
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    //CLLocationCoordinate2D loc = [userLocation coordinate];
    //    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(loc, 250, 250);
    //    [_mapView setRegion:region animated:YES];
    [self mapLocationSet:userLocation ];
    
}
-(void)mapLocationSet:(MKUserLocation*)locationn
{
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.05;
    span.longitudeDelta = 0.05;
    region.span = span;
    region.center =locationn.coordinate;
    [_mapView setCenterCoordinate:locationn.coordinate animated:YES];

    [self.mapView setRegion:region animated:YES];
}
- (void)locationManager:(CLLocationManager *)manager
         didEnterRegion:(CLRegion *)region {
    NSLog(@"didEnter : %@", region);
  //  [self displayNotif:@"Bienvenue !" withBody:@"Passez nous voir !"];
    [UserModal showAlert:@"Alert" withMessage:@"Enter" withVC:self withPop:NO];

}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
   // [self displayNotif:@"Au revoir !" withBody:@"A bientôt !"];
    [UserModal showAlert:@"Alert" withMessage:@"Exit" withVC:self withPop:NO];

}

- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region {
    NSLog(@"Start monitoring for region: %@", region.identifier);
    [self.locationManager requestStateForRegion:region];
    //[self.locationManager startMonitoringForRegion:region];

}

- (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error {
    NSLog(@"Error: %@", error);
    [UserModal showAlert:@"Alert" withMessage:@"Fial" withVC:self withPop:NO];
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations {
    NSLog(@"NEW LOCATION");
    // Stop location updates when they aren't needed anymore
    //[self.locationManager stopUpdatingLocation];
    
    // Disable background location updates when they aren't needed anymore
   // self.locationManager.allowsBackgroundLocationUpdates = NO;
}
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation{
    
}
- (void)locationManager:(CLLocationManager *)manager
      didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region {
    // When regions are initialized, see if we are already within the geofence.
    //[self.locationManager requestStateForRegion:region];

    switch (state) {
        case CLRegionStateInside:{
            
        }
            break;
        case CLRegionStateUnknown: {
            NSLog(@"Unknown");

        }
            break;
        case CLRegionStateOutside:{
            NSLog(@"Outside");

        }
            break;
        default: break;
    }
}

#pragma mark- Route Show Method

-(void)route_show_method
{
    
    dispatch_async(kBgQueue, ^{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        NSString *strUrl;
        
        strUrl=[NSString stringWithFormat:@"%@origin=30.7046,76.7179&destination=30.7333,76.7794&sensor=true&units=imperial&key=AIzaSyAoKB5MLRkG1PtBAgztN3f3_kOKRlhdH_k",kBaseUrl];
        
        
        strUrl = [strUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSData *data =[NSData dataWithContentsOfURL:[NSURL URLWithString:strUrl]];
        
        [self performSelectorOnMainThread:@selector(fetchedData1:) withObject:data waitUntilDone:YES];
    });
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)fetchedData1:(NSData *)responseData {
    NSError* error;
    
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseData //1
                          
                          options:kNilOptions
                          error:&error];
    NSArray *arrRouts=[json objectForKey:@"routes"];
    if ([arrRouts isKindOfClass:[NSArray class]]&&arrRouts.count==0) {
        //        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"didn't find direction" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        //        [alrt show];
        return;
    }
    //    NSArray *arrDistance =[[[json valueForKeyPath:@"routes.legs.steps.distance.text"] objectAtIndex:0]objectAtIndex:0];
    //    NSString *totalDuration = [[[json valueForKeyPath:@"routes.legs.duration.text"] objectAtIndex:0]objectAtIndex:0];
    
    //    NSArray *arrDescription =[[[json valueForKeyPath:@"routes.legs.steps.html_instructions"] objectAtIndex:0] objectAtIndex:0];
    //    dictRouteInfo=[NSDictionary dictionaryWithObjectsAndKeys:totalDistance,@"totalDistance",totalDuration,@"totalDuration",arrDistance ,@"distance",arrDescription,@"description", nil];
    
    NSArray* arrpolyline = [[[json valueForKeyPath:@"routes.legs.steps.polyline.points"] objectAtIndex:0] objectAtIndex:0]; //2
    double srcLat=[[[[json valueForKeyPath:@"routes.legs.start_location.lat"] objectAtIndex:0] objectAtIndex:0] doubleValue];
    double srcLong=[[[[json valueForKeyPath:@"routes.legs.start_location.lng"] objectAtIndex:0] objectAtIndex:0] doubleValue];
    double destLat=[[[[json valueForKeyPath:@"routes.legs.end_location.lat"] objectAtIndex:0] objectAtIndex:0] doubleValue];
    double destLong=[[[[json valueForKeyPath:@"routes.legs.end_location.lng"] objectAtIndex:0] objectAtIndex:0] doubleValue];
    CLLocationCoordinate2D sourceCordinate = CLLocationCoordinate2DMake(srcLat, srcLong);
    CLLocationCoordinate2D destCordinate = CLLocationCoordinate2DMake(destLat, destLong);
    
    [self addAnnotationSrcAndDestination:sourceCordinate :destCordinate];
    //    NSArray *steps=[[aary objectAtIndex:0]valueForKey:@"steps"];
    
    //    replace lines with this may work
    
    NSMutableArray *polyLinesArray =[[NSMutableArray alloc]initWithCapacity:0];
    
    for (int i = 0; i < [arrpolyline count]; i++)
    {
        NSString* encodedPoints = [arrpolyline objectAtIndex:i] ;
        MKPolyline *route = [self polylineWithEncodedString:encodedPoints];
        [polyLinesArray addObject:route];
    }
    
    [_mapView addOverlays:polyLinesArray];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}
-(void)addAnnotationSrcAndDestination :(CLLocationCoordinate2D )srcCord :(CLLocationCoordinate2D)destCord
{
    sourceAnnotation = [[MKPointAnnotation alloc]init];
    destAnnotation = [[MKPointAnnotation alloc]init];
    
    CLLocationCoordinate2D sourcecenter;
    sourcecenter.latitude=srcCord.latitude;
    sourcecenter.longitude=srcCord.longitude;
    CLLocationCoordinate2D destcenter;
    destcenter.latitude=destCord.latitude;
    destcenter.longitude=destCord.longitude;
    
    
    sourceAnnotation.coordinate=sourcecenter;
    destAnnotation.coordinate=destcenter;
    
    
    [_mapView addAnnotation:sourceAnnotation];
    [_mapView addAnnotation:destAnnotation];
    
    MKCoordinateRegion region;
    
    MKCoordinateSpan span;
    span.latitudeDelta=0.70;
    span.longitudeDelta=0.70;
    region.center=srcCord;
    region.span=span;
    
    
    
    [self zoomToFitMapAnnotations:_mapView insideArray:_mapView.annotations];
    
}


-(void)zoomToFitMapAnnotations:(MKMapView*)mapView insideArray:(NSArray*)anAnnotationArray
{
    // NSLog(@"%s", __FUNCTION__);
    if([mapView.annotations count] == 0) return;
    
    CLLocationCoordinate2D topLeftCoord;
    topLeftCoord.latitude = -90;
    topLeftCoord.longitude = 180;
    
    CLLocationCoordinate2D bottomRightCoord;
    bottomRightCoord.latitude = 90;
    bottomRightCoord.longitude = -180;
    
    for(MKPointAnnotation* annotation in anAnnotationArray)
    {
        topLeftCoord.longitude = fmin(topLeftCoord.longitude, annotation.coordinate.longitude);
        topLeftCoord.latitude = fmax(topLeftCoord.latitude, annotation.coordinate.latitude);
        
        bottomRightCoord.longitude = fmax(bottomRightCoord.longitude, annotation.coordinate.longitude);
        bottomRightCoord.latitude = fmin(bottomRightCoord.latitude, annotation.coordinate.latitude);
    }
    
    MKCoordinateRegion region;
    region.center.latitude = topLeftCoord.latitude - (topLeftCoord.latitude - bottomRightCoord.latitude) * 0.5;
    region.center.longitude = topLeftCoord.longitude + (bottomRightCoord.longitude - topLeftCoord.longitude) * 0.5;
    region.span.latitudeDelta = fabs(topLeftCoord.latitude - bottomRightCoord.latitude) * 1.1; // Add a little extra space on the sides
    region.span.longitudeDelta = fabs(bottomRightCoord.longitude - topLeftCoord.longitude) * 1.1; // Add a little extra space on the sides
    
    region = [mapView regionThatFits:region];
    [mapView setRegion:region animated:YES];
}
#pragma mark - decode map polyline

- (MKPolyline *)polylineWithEncodedString:(NSString *)encodedString {
    const char *bytes = [encodedString UTF8String];
    NSUInteger length = [encodedString lengthOfBytesUsingEncoding:NSUTF8StringEncoding];
    NSUInteger idx = 0;
    
    NSUInteger count = length / 4;
    CLLocationCoordinate2D *coords = calloc(count, sizeof(CLLocationCoordinate2D));
    NSUInteger coordIdx = 0;
    
    float latitude = 0;
    float longitude = 0;
    while (idx < length) {
        char byte = 0;
        int res = 0;
        char shift = 0;
        
        do {
            byte = bytes[idx++] - 63;
            res |= (byte & 0x1F) << shift;
            shift += 5;
        } while (byte >= 0x20);
        
        float deltaLat = ((res & 1) ? ~(res >> 1) : (res >> 1));
        latitude += deltaLat;
        
        shift = 0;
        res = 0;
        
        do {
            byte = bytes[idx++] - 0x3F;
            res |= (byte & 0x1F) << shift;
            shift += 5;
        } while (byte >= 0x20);
        float deltaLon = ((res & 1) ? ~(res >> 1) : (res >> 1));
        longitude += deltaLon;
        
        float finalLat = latitude * 1E-5;
        float finalLon = longitude * 1E-5;
        
        CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(finalLat, finalLon);
        coords[coordIdx++] = coord;
        
        if (coordIdx == count) {
            NSUInteger newCount = count + 10;
            coords = realloc(coords, newCount * sizeof(CLLocationCoordinate2D));
            count = newCount;
            
        }
    }
    
    MKPolyline *polyline = [MKPolyline polylineWithCoordinates:coords count:coordIdx];
    free(coords);
    
    return polyline;
}

-(MKOverlayRenderer*)mapView:(MKMapView*)mapView rendererForOverlay:(id<MKOverlay>) overlay{
    if ([overlay isKindOfClass:[MKCircle class]]) {
        // Draw the circle on the map how we want it (light blue inside with blue border)
        MKCircleRenderer* aRenderer = [[MKCircleRenderer alloc] initWithCircle:(MKCircle *)overlay];
        
        aRenderer.fillColor = [[UIColor blueColor] colorWithAlphaComponent:0.1];
        aRenderer.strokeColor = [[UIColor blueColor] colorWithAlphaComponent:0.4];
        aRenderer.lineWidth = 3;
        return aRenderer;
    }else{

    MKPolylineRenderer *polylineView = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
    polylineView.lineWidth = 5;
    polylineView.strokeColor = [UIColor greenColor];
    polylineView.fillColor = [[UIColor purpleColor] colorWithAlphaComponent:0.1f];
    
    return polylineView;
    }
}
-(void)updateLocation:(CLLocation*)location{
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
                   {
                       // Background work
                       NSString *lat=[NSString stringWithFormat:@"%f",location.coordinate.latitude];
                       NSString *longitute=[NSString stringWithFormat:@"%f",location.coordinate.longitude];
                       NSDictionary *dic=[[NSDictionary alloc]initWithObjectsAndKeys:[UserModal getUserId],@"tracker_id",_shipmentNumber,@"shipment_number",lat,@"latitude",longitute,@"longitude",@"Tracker location",@"tracker_location", nil];
                       [[WebService sharedInstance]parameters:dic onComplete:^(id responseObject) {
                           
                           NSLog(@"%@",responseObject);
                       } onError:^(NSError *error) {
                           NSLog(@"%@",error.localizedDescription);
                       }];
                   });
    
}

- (IBAction)completeShipmentAction:(id)sender {
    
    [self updatejobStatus:[NSString stringWithFormat:@"%ld",(long)_shipmentStatus]haltReason:@""];
    
    
    
//    NSDictionary*dic;
//    [SVProgressHUD show];

//    if ([[[sender titleLabel] text]isEqualToString:@"Pickup Shipment"]) {
//        dic=[[NSDictionary alloc]initWithObjectsAndKeys:[UserModal getUserId ],@"user_id",_shimpmentID,@"shipment_id",@"Comfirm Shipment",@"comfirm_shipment", nil];
//    }else{
//        dic=[[NSDictionary alloc]initWithObjectsAndKeys:[UserModal getUserId ],@"user_id",_shimpmentID,@"shipment_id",@"Complete Shipment",@"complete_shipment", nil];
//        }
//    [[WebService sharedInstance]parameters:dic onComplete:^(id responseObject) {
//        [SVProgressHUD dismiss];
//        if ([[responseObject valueForKey:@"status"]integerValue]==1) {
//            if ([dic objectForKey:@"comfirm_shipment"]) {
//                
//                [sender setTitle:@"Complete Shipment" forState:UIControlStateNormal];
//            }else{
//                [[NSNotificationCenter defaultCenter]postNotificationName:@"clearFeilds"object:self];
//                [self.navigationController popToRootViewControllerAnimated:YES];
//                
//            }
//            
//        }else{
//            [UserModal showAlert:@"Alert" withMessage:[responseObject valueForKey:@"message"] withVC:self withPop:NO];
//        }
//    } onError:^(NSError *error) {
//        [SVProgressHUD dismiss];
//        
//    }];

}
-(void)updatejobStatus:(NSString*)status haltReason:(NSString*)reason{
    [SVProgressHUD show];
    NSDictionary *dic=[[NSDictionary alloc]initWithObjectsAndKeys:[UserModal getUserId],@"tracker_id",_shimpmentID,@"shipment_id",status,@"status",@"update_status",@"update_all_status",reason,@"halt_reason", nil];
    [[WebService sharedInstance]parameters:dic onComplete:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([[responseObject valueForKey:@"status"]integerValue]==1) {
            [[UserModal sharedInstance]setTruckerStatusWithResposne:nil];
            if (status.intValue==1) {
                [self pushToVCWithStatus:2];
                
            }
           else if (status.intValue==2) {
                [self pushToVCWithStatus:3];
                
           }else if (status.intValue==3) {
               [self pushToVCWithStatus:4];
               
           }else if (status.intValue==4) {
               [self pushToVCWithStatus:7];
               
           }else if (status.intValue==5) {
               [self pushToVCWithStatus:6];
               
           }else if (status.intValue==6){
               [self pushToVCWithStatus:7];

           }else if (status.intValue==7){
               [self pushToVCWithStatus:8];
               
           }
           else if (status.integerValue==8) {
               [self pushToVCWithStatus:9];
               
           }else if (status.intValue==9) {
               [self pushToVCWithStatus:0];
               
           }
        }
        
        
    } onError:^(NSError *error) {
        [SVProgressHUD dismiss];
        [UserModal showAlert:@"Error!!" withMessage:error.localizedDescription withVC:self withPop:NO];
    }];
}
-(void)pushToVCWithStatus:(NSInteger)status{
    if (status==0) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }else{
        OnRouteShipmentVC *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"OnRouteShipmentVC"];
        vc.shipmentNumber=[_shipmentDetail valueForKey:@"shipment_number"];
        vc.shimpmentID=[_shipmentDetail valueForKey:@"id"];
        vc.shipmentDetail=_shipmentDetail ;
        vc.shipmentStatus=status;
        [[NSNotificationCenter defaultCenter]postNotificationName:@"clearFeilds"object:self];

        [self.navigationController pushViewController:vc animated:YES];
    }
}
- (IBAction)haultAction:(id)sender {
    _blurView.hidden=NO;
    [self.view addSubview:_blurView];
    _blurView.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [UIView animateWithDuration:0.7 delay:0.3 usingSpringWithDamping:0.5 initialSpringVelocity:1.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        _haltView.transform=CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.6 delay:0.0 usingSpringWithDamping:0.5 initialSpringVelocity:0.8 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            _closeBtn.transform=CGAffineTransformIdentity;
        }completion:^(BOOL finished) {
        }];
    }];
}
- (IBAction)submitHaltAction:(id)sender {
    if ([_haltTxtView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length==0) {
        [UserModal showAlert:@"Alert" withMessage:@"Please enter halt reason" withVC:self withPop:NO];
    }else{
        [self updatejobStatus:@"5" haltReason:_haltTxtView.text];

    }
}
- (IBAction)closeAction:(id)sender {
    _closeBtn.transform=CGAffineTransformMakeScale(0.0, 0.0);
    _haltView.transform=CGAffineTransformMakeScale(0.0, 0.0);
    _blurView.hidden=YES;
}
@end
