//
//  ShipmentDetailVC.m
//  Dingo
//
//  Created by mac on 7/14/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import "ShipmentDetailVC.h"
#import "TrackShipmentVC.h"
#import "UserModal.h"
#import "SVProgressHUD.h"
#import "WebService.h"
#import "OnRouteShipmentVC.h"
#import "AgreementVC.h"
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,0)
#define kBaseUrl @"https://maps.googleapis.com/maps/api/directions/json?"
@interface ShipmentDetailVC ()
{
    MKPointAnnotation *sourceAnnotation;
    MKPointAnnotation *destAnnotation;
}
@end

@implementation ShipmentDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self route_show_method];
    // Do any additional setup after loading the view.
    _sourceLbl.text=[_shipmentDeatil valueForKey:@"pickup_address"];
    _destinationLbl.text=[_shipmentDeatil valueForKey:@"dropoff_address"];
    _dateLbl.text=[_shipmentDeatil valueForKey:@"pickup_window"];
    _shipmentNumberLbl.text=[_shipmentDeatil valueForKey:@"shipment_number"];
    _loadSizeLbl.text=[_shipmentDeatil valueForKey:@"load_size"];
    _shipmentNameLbl.text=[_shipmentDeatil valueForKey:@"shipment_name"];
    _shipmentPriceLbl.text=[NSString stringWithFormat:@"$ %ld",(long)[[_shipmentDeatil valueForKey:@"shipper_cost"] integerValue]];
    _trackBtn.layer.cornerRadius=_trackBtn.frame.size.height/2;
    _vview.layer.cornerRadius=15.0;
    if ([[_shipmentDeatil valueForKey:@"shipment_status"] intValue]<3){
        _trackBtn.hidden=YES;
    }
    if (_trucker) {
        _trackBtn.hidden=NO;
        [_trackBtn setTitle:@"Go To Agreement" forState:UIControlStateNormal];
    }
}
-(void)viewWillLayoutSubviews{
    [UserModal addGradient:_trackBtn];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)trackAction:(id)sender {
    if (_trucker) {
       [self pickJob];
//        AgreementVC *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"AgreementVC"];
//         [self.navigationController pushViewController:vc animated:YES];

        
        
    }else{
    TrackShipmentVC *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"TrackShipmentVC"];
    vc.shipmentNumber=[_shipmentDeatil valueForKey:@"shipment_number"];
    vc.fromHome=YES;
    [self.navigationController pushViewController:vc animated:YES];
    }
}
#pragma mark- Route Show Method

-(void)route_show_method
{
    
    dispatch_async(kBgQueue, ^{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        NSString *strUrl;
        
        strUrl=[NSString stringWithFormat:@"%@origin=%@,%@&destination=%@,%@&sensor=true&units=imperial&key=AIzaSyAoKB5MLRkG1PtBAgztN3f3_kOKRlhdH_k",kBaseUrl,[_shipmentDeatil valueForKey:@"pickup_latitude"],[_shipmentDeatil valueForKey:@"pickup_longitude"],[_shipmentDeatil valueForKey:@"dropoff_latitude"],[_shipmentDeatil valueForKey:@"dropoff_longitute"]];
        
       
        strUrl = [strUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSData *data =[NSData dataWithContentsOfURL:[NSURL URLWithString:strUrl]];
        
        [self performSelectorOnMainThread:@selector(fetchedData1:) withObject:data waitUntilDone:YES];
    });
    
    
}

- (void)fetchedData1:(NSData *)responseData {
    NSError* error;
    
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseData //1
                          
                          options:kNilOptions
                          error:&error];
    NSArray *arrRouts=[json objectForKey:@"routes"];
    if ([arrRouts isKindOfClass:[NSArray class]]&&arrRouts.count==0) {
        //        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"didn't find direction" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        //        [alrt show];
        return;
    }
   
    
    
    NSArray* arrpolyline = [[[json valueForKeyPath:@"routes.legs.steps.polyline.points"] objectAtIndex:0] objectAtIndex:0]; //2
    double srcLat=[[[[json valueForKeyPath:@"routes.legs.start_location.lat"] objectAtIndex:0] objectAtIndex:0] doubleValue];
    double srcLong=[[[[json valueForKeyPath:@"routes.legs.start_location.lng"] objectAtIndex:0] objectAtIndex:0] doubleValue];
    double destLat=[[[[json valueForKeyPath:@"routes.legs.end_location.lat"] objectAtIndex:0] objectAtIndex:0] doubleValue];
    double destLong=[[[[json valueForKeyPath:@"routes.legs.end_location.lng"] objectAtIndex:0] objectAtIndex:0] doubleValue];
    CLLocationCoordinate2D sourceCordinate = CLLocationCoordinate2DMake(srcLat, srcLong);
    CLLocationCoordinate2D destCordinate = CLLocationCoordinate2DMake(destLat, destLong);
    
    [self addAnnotationSrcAndDestination:sourceCordinate :destCordinate];
    //    NSArray *steps=[[aary objectAtIndex:0]valueForKey:@"steps"];
    
    //    replace lines with this may work
    
    NSMutableArray *polyLinesArray =[[NSMutableArray alloc]initWithCapacity:0];
    
    for (int i = 0; i < [arrpolyline count]; i++)
    {
        NSString* encodedPoints = [arrpolyline objectAtIndex:i] ;
        MKPolyline *route = [self polylineWithEncodedString:encodedPoints];
        [polyLinesArray addObject:route];
    }
    
    [_mapView addOverlays:polyLinesArray];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}
-(void)addAnnotationSrcAndDestination :(CLLocationCoordinate2D )srcCord :(CLLocationCoordinate2D)destCord
{
    sourceAnnotation = [[MKPointAnnotation alloc]init];
    destAnnotation = [[MKPointAnnotation alloc]init];
    
    CLLocationCoordinate2D sourcecenter;
    sourcecenter.latitude=srcCord.latitude;
    sourcecenter.longitude=srcCord.longitude;
    CLLocationCoordinate2D destcenter;
    destcenter.latitude=destCord.latitude;
    destcenter.longitude=destCord.longitude;
    
    
    sourceAnnotation.coordinate=sourcecenter;
    destAnnotation.coordinate=destcenter;
    
    
    [_mapView addAnnotation:sourceAnnotation];
    [_mapView addAnnotation:destAnnotation];
    
    MKCoordinateRegion region;
    
    MKCoordinateSpan span;
    span.latitudeDelta=0.50;
    span.longitudeDelta=0.50;
    region.center=srcCord;
    region.span=span;
    
    
    
    [self zoomToFitMapAnnotations:_mapView insideArray:_mapView.annotations];
    
}


-(void)zoomToFitMapAnnotations:(MKMapView*)mapView insideArray:(NSArray*)anAnnotationArray
{
    // NSLog(@"%s", __FUNCTION__);
    if([mapView.annotations count] == 0) return;
    
    CLLocationCoordinate2D topLeftCoord;
    topLeftCoord.latitude = -90;
    topLeftCoord.longitude = 180;
    
    CLLocationCoordinate2D bottomRightCoord;
    bottomRightCoord.latitude = 90;
    bottomRightCoord.longitude = -180;
    
    for(MKPointAnnotation* annotation in anAnnotationArray)
    {
        topLeftCoord.longitude = fmin(topLeftCoord.longitude, annotation.coordinate.longitude);
        topLeftCoord.latitude = fmax(topLeftCoord.latitude, annotation.coordinate.latitude);
        
        bottomRightCoord.longitude = fmax(bottomRightCoord.longitude, annotation.coordinate.longitude);
        bottomRightCoord.latitude = fmin(bottomRightCoord.latitude, annotation.coordinate.latitude);
    }
    
    MKCoordinateRegion region;
    region.center.latitude = topLeftCoord.latitude - (topLeftCoord.latitude - bottomRightCoord.latitude) * 0.5;
    region.center.longitude = topLeftCoord.longitude + (bottomRightCoord.longitude - topLeftCoord.longitude) * 0.5;
    region.span.latitudeDelta = fabs(topLeftCoord.latitude - bottomRightCoord.latitude) * 1.1; // Add a little extra space on the sides
    region.span.longitudeDelta = fabs(bottomRightCoord.longitude - topLeftCoord.longitude) * 1.1; // Add a little extra space on the sides
    
    region = [mapView regionThatFits:region];
    [mapView setRegion:region animated:YES];
}
#pragma mark - decode map polyline

- (MKPolyline *)polylineWithEncodedString:(NSString *)encodedString {
    const char *bytes = [encodedString UTF8String];
    NSUInteger length = [encodedString lengthOfBytesUsingEncoding:NSUTF8StringEncoding];
    NSUInteger idx = 0;
    
    NSUInteger count = length / 4;
    CLLocationCoordinate2D *coords = calloc(count, sizeof(CLLocationCoordinate2D));
    NSUInteger coordIdx = 0;
    
    float latitude = 0;
    float longitude = 0;
    while (idx < length) {
        char byte = 0;
        int res = 0;
        char shift = 0;
        
        do {
            byte = bytes[idx++] - 63;
            res |= (byte & 0x1F) << shift;
            shift += 5;
        } while (byte >= 0x20);
        
        float deltaLat = ((res & 1) ? ~(res >> 1) : (res >> 1));
        latitude += deltaLat;
        
        shift = 0;
        res = 0;
        
        do {
            byte = bytes[idx++] - 0x3F;
            res |= (byte & 0x1F) << shift;
            shift += 5;
        } while (byte >= 0x20);
        float deltaLon = ((res & 1) ? ~(res >> 1) : (res >> 1));
        longitude += deltaLon;
        
        float finalLat = latitude * 1E-5;
        float finalLon = longitude * 1E-5;
        
        CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(finalLat, finalLon);
        coords[coordIdx++] = coord;
        
        if (coordIdx == count) {
            NSUInteger newCount = count + 10;
            coords = realloc(coords, newCount * sizeof(CLLocationCoordinate2D));
            count = newCount;
            
        }
    }
    
    MKPolyline *polyline = [MKPolyline polylineWithCoordinates:coords count:coordIdx];
    free(coords);
    
    return polyline;
}
-(MKOverlayRenderer*)mapView:(MKMapView*)mapView rendererForOverlay:(id<MKOverlay>) overlay{
    
    MKPolylineRenderer *polylineView = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
    polylineView.lineWidth = 5;
    polylineView.strokeColor = [UIColor colorWithRed:50/255.0 green:176/255.0 blue:255/255.0 alpha:1.0];
    polylineView.fillColor = [[UIColor purpleColor] colorWithAlphaComponent:0.1f];
    
    return polylineView;
}
#pragma mark- webservice
-(void)pickJob{
    [SVProgressHUD show];
    NSDictionary *dic=[[NSDictionary alloc]initWithObjectsAndKeys:[UserModal getUserId],@"user_id",[_shipmentDeatil valueForKey:@"id"],@"shipment_id",@"Pick Shipment",@"pick_shipment", nil];
    [[WebService sharedInstance]parameters:dic onComplete:^(id responseObject) {
        [SVProgressHUD dismiss];
        
        if ([[responseObject valueForKey:@"status"]integerValue]==1) {
            
            //   [UserModal showAlert:@"Message" withMessage:@"Successfully picked the job" withVC:self withPop:YES];
            OnRouteShipmentVC *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"OnRouteShipmentVC"];
            vc.shipmentNumber=[_shipmentDeatil valueForKey:@"shipment_number"];
            vc.shimpmentID=[_shipmentDeatil valueForKey:@"id"];
            vc.shipmentDetail=_shipmentDeatil ;
            vc.shipmentStatus=2;
            [self.navigationController pushViewController:vc animated:YES];
            
        }else{
            [UserModal showAlert:@"Alert" withMessage:[responseObject valueForKey:@"message"] withVC:self withPop:NO];
        }
    } onError:^(NSError *error) {
        [SVProgressHUD dismiss];
        [UserModal showAlert:@"Error!!" withMessage:error.localizedDescription withVC:self withPop:NO];

    }];
}
@end
