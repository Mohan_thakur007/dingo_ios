//
//  TruckerSelectShipmentCell.h
//  Dingo
//
//  Created by MAC on 06/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TruckerSelectShipmentCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIView *cellView;
@property (strong, nonatomic) IBOutlet UIButton *selectionBtn;
@property (strong, nonatomic) IBOutlet UILabel *timeLbl;
@property (strong, nonatomic) IBOutlet UILabel *priceLbl;

@property (strong, nonatomic) IBOutlet UILabel *pickupLbl;
@end
