//
//  MenuVC.m
//  Dingo
//
//  Created by MAC on 05/06/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import "MenuVC.h"
#import "UIViewController+RESideMenu.h"
#import "RESideMenu.h"
#import "UserModal.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface MenuVC ()
{
    NSArray *items,*truckerItems,*imagesArr,*truckerimages;
}
@end

@implementation MenuVC

- (void)viewDidLoad {
    [super viewDidLoad];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.view.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[UIColor colorWithRed:230/255.0 green:228/255.0 blue:228/255.0 alpha:0.5].CGColor, (id)[UIColor colorWithRed:50/255.0 green:176/255.0 blue:255/255.0 alpha:0.5].CGColor, nil];
    gradient.startPoint = CGPointZero;
    gradient.endPoint = CGPointMake(1, 1);
    
    [self.view.layer insertSublayer:gradient atIndex:0];
    items=@[@"Home",@"Shipments",@"Track Shipment",@"Settings",@"Feedback",@"Logout"];
     truckerItems=@[@"Home",@"Upload Bills",@"Settings",@"Feedback",@"Logout"];
    truckerimages=@[@"home",@"upload_",@"settings_white",@"feedback",@"logout"];

    imagesArr=@[@"home",@"truck_white",@"track",@"settings_white",@"feedback",@"logout"];
    // Do any additional setup after loading the view.
      _profilePic.layer.cornerRadius=self.view.frame.size.height*0.07;
    _profilePic.clipsToBounds=YES;
}
-(void)viewWillAppear:(BOOL)animated{
    _userNameLbl.text=[NSString stringWithFormat:@"%@ %@",[UserModal getFirstName],[UserModal getLastname]];
    [_profilePic sd_setImageWithURL:[NSURL URLWithString:[UserModal getProfilePic]]placeholderImage:[UIImage imageNamed:@"defaultpic"]];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- UITableView Datasource


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    if ([[UserModal getUserType] integerValue]==2) {
        return truckerItems.count;

    }
    return items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
      if ([[UserModal getUserType] integerValue]==2) {
        [(UILabel*)[cell viewWithTag:5] setText:[truckerItems objectAtIndex:indexPath.row]];
        [(UIImageView*)[cell viewWithTag:52]setImage:[UIImage imageNamed:[truckerimages objectAtIndex:indexPath.row]]];
        
    }else{
        [(UILabel*)[cell viewWithTag:5] setText:[items objectAtIndex:indexPath.row]];
        [(UIImageView*)[cell viewWithTag:52]setImage:[UIImage imageNamed:[imagesArr objectAtIndex:indexPath.row]]];
    }
    return cell;
}
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([[UserModal getUserType] integerValue]==2){
        switch (indexPath.row) {
                
            case 0:
            {
                [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"DashboardVC"]]
                                                             animated:YES];
                [self.sideMenuViewController hideMenuViewController];
            }
                break;
            case 1:
            {
                [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"UploadBillsVC"]]
                                                             animated:YES];
                [self.sideMenuViewController hideMenuViewController];
            }
                break;
            case 2:
            {
                [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"SettingVC"]]
                                                             animated:YES];
                [self.sideMenuViewController hideMenuViewController];
            }
                
            case 3:
            {
               
            }
                break;
            case 4:
            {
                [self.sideMenuViewController hideMenuViewController];
                [self openAlertAndSetRootViewAfterLogout];

            }
                break;
                
            default:
                break;
                
        }
    }else{
        switch (indexPath.row) {
                
            case 0:
            {
                [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"DashboardVC"]]
                                                             animated:YES];
                [self.sideMenuViewController hideMenuViewController];
            }
                break;
            case 1:
            {
                [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"ShipmentsVC"]]
                                                             animated:YES];
                [self.sideMenuViewController hideMenuViewController];
            }
                break;
            case 2:
            {
                [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"TrackShipmentVC"]]
                                                             animated:YES];
                [self.sideMenuViewController hideMenuViewController];
            }
               break;
            case 3:
            {
                [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"SettingVC"]]
                                                             animated:YES];
                [self.sideMenuViewController hideMenuViewController];
            }
                break;
          

            case 5:
            {
                 [self.sideMenuViewController hideMenuViewController];
                [self openAlertAndSetRootViewAfterLogout];
            }
                break;
                
            default:
                break;
        }
    }
}

-(void)openAlertAndSetRootViewAfterLogout{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil message:@"Are you sure you want to logout?" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Logout" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action){
        [self resetDefaults];
        UIViewController *homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        
        UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:homeViewController];
        
        [UIView transitionWithView:[UIApplication sharedApplication].keyWindow
                          duration:0.5
                           options:UIViewAnimationOptionTransitionFlipFromLeft
                        animations:^{ [UIApplication sharedApplication].keyWindow.rootViewController=nav; }
                        completion:nil];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action){
        
    }]];
    
    [self presentViewController:alert animated:YES completion:nil];
}
- (void)resetDefaults {
    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
    NSDictionary * dict = [defs dictionaryRepresentation];
    for (id key in dict) {
        if ([key isEqualToString:@"email1"]||[key isEqualToString:@"pass"]) {
            
        }else{
            [defs removeObjectForKey:key];
            
        }
    }
    [defs synchronize];
}

@end
